﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using Respawn;
using System.IO;
using WAMG.Infrastructure.Persistence;
using WAMG.WebAPI;

namespace WAMG.IntegrationTests
{
    [SetUpFixture]
    public class TestingFixture
    {

        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            SetUpConfiguration();
            SetUpStartupAndScopeFactory();
            SetUpDatabaseAndCheckpoint();
        }

        private static void SetUpConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("appsettings.Staging.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables(prefix: "WAMG_TESTS_");

            var configuration = builder.Build();

            TestingHelpers.SetConfiguration(configuration);
        }

        private static void SetUpStartupAndScopeFactory()
        {
            var startup = new Startup(TestingHelpers.Configuration);

            var services = new ServiceCollection();

            services.AddSingleton(Mock.Of<IWebHostEnvironment>(w => w.EnvironmentName == "Development"));

            services.AddLogging();

            services.AddSingleton<IConfiguration>(TestingHelpers.Configuration);

            startup.ConfigureServices(services);

            var scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();

            TestingHelpers.SetScopeFactory(scopeFactory);
        }

        private static void SetUpDatabaseAndCheckpoint()
        {
            using var scope = TestingHelpers.CreateScope();

            using var context = TestingHelpers.GetService<ApplicationDbContext>(scope);
            if (!context.Database.IsInMemory())
            {
                context.Database.EnsureDeleted();
                context.Database.Migrate();
            }

            using var identityContext = TestingHelpers.GetService<ApplicationIdentityDbContext>(scope);
            if (!identityContext.Database.IsInMemory())
            {
                identityContext.Database.Migrate();
            }

            var checkpoint = new Checkpoint
            {
                DbAdapter = DbAdapter.MySql,
                TablesToIgnore = new[] { "__EFMigrationsHistory" },
            };

            TestingHelpers.SetCheckpoint(checkpoint);
        }
    }
}
