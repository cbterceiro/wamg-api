﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAMG.Application.Queries.GamingPlatforms.GetGamingPlatforms;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Queries.GamingPlatforms
{
    using static FluentActions;
    using static TestingHelpers;

    public class GetGamingPlatformsTests : TestBase
    {
        private static readonly GamingPlatform[] GamingPlatformsDatabaseSetup = new[]
        {
            new GamingPlatform() { Id = 1, Name = "Playstation 5", IsConsole = true, IsHandheld = false },
            new GamingPlatform() { Id = 2, Name = "Xbox Series X", IsConsole = true, IsHandheld = false },
            new GamingPlatform() { Id = 3, Name = "Nintendo Switch", IsConsole = true, IsHandheld = true },
            new GamingPlatform() { Id = 4, Name = "Computer", IsConsole = false, IsHandheld = false },
        };

        [SetUp]
        public async Task PrepareDatabaseWithGamingPlatforms()
        {
            await PrepareDatabaseWith(GamingPlatformsDatabaseSetup);
        }

        [Test]
        public void ShouldValidateNameExceeding100Characters()
        {
            var query = new GetGamingPlatformsQuery()
            {
                Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded",
            };

            SendQueryShouldThrowValidationExceptionWith(query, ErrorDictionaryFrom("Name", "The name query filter must not exceed 100 characters"));
        }

        [Test]
        public void ShouldNotThrowExceptionWithNoArgumentsProvided()
        {
            var query = new GetGamingPlatformsQuery();

            Invoking(() => SendQueryAsync(query))
                .Should()
                .NotThrow();
        }

        [Test]
        public async Task ShouldGetAllGamingPlatforms()
        {
            var query = new GetGamingPlatformsQuery();

            var result = await SendQueryAsync(query);

            result.Should().HaveCount(GamingPlatformsDatabaseSetup.Length);
            result.Select(r => r.Id).Should().BeEquivalentTo(GamingPlatformsDatabaseSetup.Select(g => g.Id).ToList());
        }

        [Test]
        public async Task ShouldFilterByNameSuccessfully()
        {
            var query = new GetGamingPlatformsQuery()
            {
                Name = "station",
            };

            var result = await SendQueryAsync(query);
            result.Should().HaveCount(1);
            result.First().Name.Should().Be("Playstation 5");
        }

        [Test]
        public async Task ShouldGetOnlyConsoles()
        {
            var query = new GetGamingPlatformsQuery()
            {
                OnlyConsole = true,
            };

            var result = await SendQueryAsync(query);
            result.Should().NotBeEmpty();
            result.Select(r => r.IsConsole).Should().NotContain(false);
        }

        [Test]
        public async Task ShouldGetOnlyHandhelds()
        {
            var query = new GetGamingPlatformsQuery()
            {
                OnlyHandheld = true,
            };

            var result = await SendQueryAsync(query);
            result.Should().NotBeEmpty();
            result.Select(r => r.IsHandheld).Should().NotContain(false);
        }

        [Test]
        public async Task ShouldGetOnlyHandheldConsoles()
        {
            var query = new GetGamingPlatformsQuery()
            {
                OnlyConsole = true,
                OnlyHandheld = true,
            };

            var result = await SendQueryAsync(query);
            result.Should().NotBeEmpty();
            result.Select(r => r.IsConsole && r.IsHandheld).Should().NotContain(false);
        }

        [Test]
        public async Task ShouldOrderGamingPlatformsByName()
        {
            var query = new GetGamingPlatformsQuery();

            var result = await SendQueryAsync(query);

            result.Should().NotBeEmpty();
            result.Select(r => r.Name).Should().BeInAscendingOrder();
        }
    }
}
