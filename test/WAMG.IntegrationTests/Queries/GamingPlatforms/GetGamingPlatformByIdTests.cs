﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Application.Queries.GamingPlatforms.GetGamingPlatformById;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Queries.GamingPlatforms
{
    using static FluentActions;
    using static TestingHelpers;

    public class GetGamingPlatformByIdTests : TestBase
    {
        private readonly GamingPlatform TestGamingPlatform = new GamingPlatform()
        {
            Id = 1,
            Name = "Playstation 5",
            IsConsole = true,
            IsHandheld = false,
            CreatedAt = DateTime.Now,
        };

        [SetUp]
        public async Task PrepareDatabaseWithGamingPlatform()
        {
            await PrepareDatabaseWith(new[] { TestGamingPlatform });
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var query = new GetGamingPlatformByIdQuery(999);

            Invoking(() => SendCommandAsync(query))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity GamingPlatform (999) was not found");
        }

        [Test]
        public async Task ShouldFindEntityByIdSuccessfully()
        {
            var query = new GetGamingPlatformByIdQuery(TestGamingPlatform.Id);

            var result = await SendQueryAsync(query);

            result.Id.Should().Be(TestGamingPlatform.Id);
            result.Name.Should().Be(TestGamingPlatform.Name);
            result.IsConsole.Should().Be(TestGamingPlatform.IsConsole);
            result.IsHandheld.Should().Be(TestGamingPlatform.IsHandheld);
        }
    }
}
