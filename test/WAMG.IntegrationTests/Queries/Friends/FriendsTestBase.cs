﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Queries.Friends
{
    using static TestingHelpers;

    public class FriendsTestBase : TestBase
    {
        [SetUp]
        public async Task PrepareDatabaseWithFriends()
        {
            await PrepareDatabaseWith(FriendsDatabaseSetup);
        }

        protected static readonly Friend[] FriendsDatabaseSetup = new[]
        {
            new Friend() { Id = 1, Name = "The first one", Email = "some@email.com", PhoneNumber = "+5527987654321" },
            new Friend() { Id = 2, Name = "A good friend" },
            new Friend() { Id = 3, Name = "Some friend" },
            new Friend() { Id = 4, Name = "Another friend" },
            new Friend() { Id = 5, Name = "Keanu Reeves" },
            new Friend() { Id = 6, Name = "John Wick" },
            new Friend() { Id = 7, Name = "Neo" },
        };

        protected static readonly Friend FirstFriendOfDatabaseSetup = FriendsDatabaseSetup[0];

        protected static readonly List<GetFriendsWithPaginationQueryTestCaseData> PaginationSuccessTestCases = new List<GetFriendsWithPaginationQueryTestCaseData>()
        {
            new GetFriendsWithPaginationQueryTestCaseData()
            {
                PageNumber = 1,
                PageSize = 3,
                ExpectedItemsCount = 3,
                ExpectedTotalPages = 3,
                ExpectedTotalCount = 7,
                ExpectedHasPreviousPage = false,
                ExpectedHasNextPage = true
            },
            new GetFriendsWithPaginationQueryTestCaseData()
            {
                PageNumber = 2,
                PageSize = 3,
                ExpectedItemsCount = 3,
                ExpectedTotalPages = 3,
                ExpectedTotalCount = 7,
                ExpectedHasPreviousPage = true,
                ExpectedHasNextPage = true
            },
            new GetFriendsWithPaginationQueryTestCaseData()
            {
                PageNumber = 3,
                PageSize = 3,
                ExpectedItemsCount = 1,
                ExpectedTotalPages = 3,
                ExpectedTotalCount = 7,
                ExpectedHasPreviousPage = true,
                ExpectedHasNextPage = false
            },
            new GetFriendsWithPaginationQueryTestCaseData()
            {
                PageNumber = 1,
                PageSize = 5,
                ExpectedItemsCount = 5,
                ExpectedTotalPages = 2,
                ExpectedTotalCount = 7,
                ExpectedHasPreviousPage = false,
                ExpectedHasNextPage = true
            },
            new GetFriendsWithPaginationQueryTestCaseData()
            {
                PageNumber = 2,
                PageSize = 5,
                ExpectedItemsCount = 2,
                ExpectedTotalPages = 2,
                ExpectedTotalCount = 7,
                ExpectedHasPreviousPage = true,
                ExpectedHasNextPage = false
            },
        };
    }


    public class GetFriendsWithPaginationQueryTestCaseData
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int ExpectedItemsCount { get; set; }
        public int ExpectedTotalPages { get; set; }
        public int ExpectedTotalCount { get; set; }
        public bool ExpectedHasPreviousPage { get; set; }
        public bool ExpectedHasNextPage { get; set; }
    }
}
