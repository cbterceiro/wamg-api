﻿using FluentAssertions;
using NUnit.Framework;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Application.Queries.Friends.GetFriendById;

namespace WAMG.IntegrationTests.Queries.Friends
{
    using static FluentActions;
    using static TestingHelpers;

    public class GetFriendByIdTests : FriendsTestBase
    {

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var query = new GetFriendByIdQuery(999);

            Invoking(() => SendQueryAsync(query))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity Friend (999) was not found");            
        }

        [Test]
        public async Task ShouldFindEntityByIdSuccessfully()
        {
            var query = new GetFriendByIdQuery(FirstFriendOfDatabaseSetup.Id);

            var result = await SendQueryAsync(query);

            result.Id.Should().Be(FirstFriendOfDatabaseSetup.Id);
            result.Name.Should().Be(FirstFriendOfDatabaseSetup.Name);
            result.Email.Should().Be(FirstFriendOfDatabaseSetup.Email);
            result.PhoneNumber.Should().Be(FirstFriendOfDatabaseSetup.PhoneNumber);
        }
        
    }
}
