﻿using FluentAssertions;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Application.Queries.Friends.GetFriendsWithPagination;

namespace WAMG.IntegrationTests.Queries.Friends
{
    using static FluentActions;
    using static TestingHelpers;

    public class GetFriendsWithPaginationTests : FriendsTestBase
    {
        [Test]
        public void ShouldValidatePageNumberNotLowerThanOne()
        {
            var query = new GetFriendsWithPaginationQuery()
            {
                PageNumber = 0,
            };

            SendQueryShouldThrowValidationExceptionWith(query, ErrorDictionaryFrom("PageNumber", "The page number must be greater than or equal to 1"));
        }

        [Test]
        [TestCase(0)]
        [TestCase(101)]
        public void ShouldValidatePageSizeBetweenOneAndOneHundred(int pageSize)
        {
            var query = new GetFriendsWithPaginationQuery()
            {
                PageSize = pageSize,
            };

            SendQueryShouldThrowValidationExceptionWith(query, ErrorDictionaryFrom("PageSize", "The page size must be between 1 and 100 items"));
        }

        [Test]
        public void ShouldValidateNameExceeding100Characters()
        {
            var query = new GetFriendsWithPaginationQuery()
            {
                Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded",
            };

            SendQueryShouldThrowValidationExceptionWith(query, ErrorDictionaryFrom("Name", "The name query filter must not exceed 100 characters"));
        }

        [Test]
        public void ShouldNotThrowExceptionWithNoArgumentsProvided()
        {
            var query = new GetFriendsWithPaginationQuery();

            Invoking(() => SendQueryAsync(query))
                .Should()
                .NotThrow();
        }

        [Test]
        [TestCaseSource(nameof(PaginationSuccessTestCases))]
        public async Task ShouldGetFriendsWithPaginationSuccesfully(GetFriendsWithPaginationQueryTestCaseData testCase)
        {
            var pageNumber = testCase.PageNumber;
            var pageSize = testCase.PageSize;
            var expectedItemsCount = testCase.ExpectedItemsCount;
            var expectedTotalPages = testCase.ExpectedTotalPages;
            var expectedTotalCount = testCase.ExpectedTotalCount;
            var expectedHasPreviousPage = testCase.ExpectedHasPreviousPage;
            var expectedHasNextPage = testCase.ExpectedHasNextPage;

            var query = new GetFriendsWithPaginationQuery()
            {
                PageNumber = pageNumber,
                PageSize = pageSize,
            };

            var result = await SendQueryAsync(query);

            result.PageNumber.Should().Be(pageNumber);
            result.PageSize.Should().Be(pageSize);
            result.Items.Should().HaveCount(expectedItemsCount);
            result.TotalPages.Should().Be(expectedTotalPages);
            result.TotalCount.Should().Be(expectedTotalCount);
            result.HasPreviousPage.Should().Be(expectedHasPreviousPage);
            result.HasNextPage.Should().Be(expectedHasNextPage);
        }

        [Test]
        public async Task ShouldFilterByNameCorrectly()
        {
            var theStringFriend = "friend";
            var friendsOnSourceWhoseNameContainsTheStringFriend = FriendsDatabaseSetup.Where(f => f.Name.Contains(theStringFriend)).ToList();

            var expectedNumberOfFilteredFriends = friendsOnSourceWhoseNameContainsTheStringFriend.Count;

            var query = new GetFriendsWithPaginationQuery()
            {
                PageNumber = 1,
                PageSize = 100,
                Name = theStringFriend,
            };

            var result = await SendQueryAsync(query);

            result.TotalCount.Should().Be(expectedNumberOfFilteredFriends);
            result.Items.Should()
                .HaveCount(expectedNumberOfFilteredFriends)
                .And.OnlyContain(f => f.Name.Contains(theStringFriend));
        }

        [Test]
        public async Task ShouldReturnFriendsOrderedByName()
        {
            var query = new GetFriendsWithPaginationQuery();

            var result = await SendQueryAsync(query);

            result.Items.Should().BeInAscendingOrder(f => f.Name);
        }
    }
}
