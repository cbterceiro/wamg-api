﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Application.Queries.Games.GetGamesWithPagination;
using WAMG.Domain.Entities;
using WAMG.Domain.Enums;

namespace WAMG.IntegrationTests.Queries.Games
{
    using static FluentActions;
    using static TestingHelpers;

    public class GetGamesWithPaginationTests : TestBase
    {
        private static readonly Dictionary<string, GamingPlatform> GamingPlatforms = new Dictionary<string, GamingPlatform>
        {
            { "PS5", new GamingPlatform() { Id = 1, Name = "Playstation 5", IsConsole = true, IsHandheld = false } },
            { "Xbox", new GamingPlatform() { Id = 2, Name = "Xbox Series X", IsConsole = true, IsHandheld = false } },
            { "Switch", new GamingPlatform() { Id = 3, Name = "Nintendo Switch", IsConsole = true, IsHandheld = true } },
            { "Computer", new GamingPlatform() { Id = 4, Name = "Computer", IsConsole = false, IsHandheld = false } },
        };
        private static readonly Dictionary<string, Friend> Friends = new Dictionary<string, Friend>
        {
            { "Bill", new Friend() { Id = 1, Name = "Bill Gates", Email = "bill@gates.com", PhoneNumber = "+5527987654321" } },
            { "Steve", new Friend() { Id = 2, Name = "Steve Jobs", Email = "steve@jobs.com", PhoneNumber = "+5527987654321" } },
            { "Mark", new Friend() { Id = 3, Name = "Mark Zuckerberg", Email = "mark@zuckerberg.com", PhoneNumber = "+5527987654321" } },
        };

        private const int TotalNumberOfGames = 8;
        private static readonly Dictionary<string, Game> GamesDatabaseSetup = new Dictionary<string, Game>
        {
            { "Demon's Souls",   new Game() { Id = 1, Name = "Demon's Souls",             Genre = GameGenre.RPG,       Platform = GamingPlatforms["PS5"],      LoanedTo = null } },
            { "Horizon",         new Game() { Id = 2, Name = "Horizon II Forbidden West", Genre = GameGenre.Action,    Platform = GamingPlatforms["PS5"],      LoanedTo = null } },
            { "Mass Effect",     new Game() { Id = 3, Name = "Mass Effect Trilogy",       Genre = GameGenre.RPG,       Platform = GamingPlatforms["Computer"], LoanedTo = Friends["Bill"] } },
            { "Crash",           new Game() { Id = 4, Name = "Crash Bandicoot 4",         Genre = GameGenre.Adventure, Platform = GamingPlatforms["Xbox"],     LoanedTo = null } },
            { "Detroit",         new Game() { Id = 5, Name = "Detroit Become Human",      Genre = GameGenre.Adventure, Platform = GamingPlatforms["PS5"],      LoanedTo = Friends["Mark"] } },
            { "Death Stranding", new Game() { Id = 6, Name = "Death Stranding",           Genre = GameGenre.Survival,  Platform = GamingPlatforms["Computer"], LoanedTo = Friends["Bill"] } },
            { "Minecraft",       new Game() { Id = 7, Name = "Minecraft",                 Genre = GameGenre.Sandbox,   Platform = GamingPlatforms["Xbox"],     LoanedTo = Friends["Bill"] } },
            { "Zelda",           new Game() { Id = 8, Name = "The Legend of Zelda",       Genre = GameGenre.Adventure, Platform = GamingPlatforms["Switch"],   LoanedTo = null } },
        };

        [SetUp]
        public async Task PrepareDatabaseWithGames()
        {
            await PrepareDatabaseWith(GamesDatabaseSetup.Values.ToArray());
        }


        [Test]
        public void ShouldValidateNameExceeding100Characters()
        {
            var query = new GetGamesWithPaginationQuery()
            {
                Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded",
            };

            SendQueryShouldThrowValidationExceptionWith(query, ErrorDictionaryFrom("Name", "The name query filter must not exceed 100 characters"));
        }

        [Test]
        public void ShouldValidateGamingPlatformIdLowerThanOne()
        {
            var query = new GetGamesWithPaginationQuery()
            {
                GamingPlatformId = 0,
            };

            SendQueryShouldThrowValidationExceptionWith(query, ErrorDictionaryFrom("GamingPlatformId", "The gaming platform identifier must be greater than or equal to 1"));
        }

        [Test]
        public void ShouldValidateGamingFriendIdLowerThanOne()
        {
            var query = new GetGamesWithPaginationQuery()
            {
                FriendId = 0,
            };

            SendQueryShouldThrowValidationExceptionWith(query, ErrorDictionaryFrom("FriendId", "The friend identifier must be greater than or equal to 1"));
        }

        [Test]
        public void ShouldNotThrowExceptionWithNoArgumentsProvided()
        {
            var query = new GetGamesWithPaginationQuery();

            Invoking(() => SendQueryAsync(query))
                .Should()
                .NotThrow();
        }

        [Test]
        [TestCase("cra",   new string[] { "Crash Bandicoot 4", "Minecraft" })]
        [TestCase("Zelda", new string[] { "The Legend of Zelda" })]
        [TestCase("RI",    new string[] { "Horizon II Forbidden West", "Mass Effect Trilogy" })]
        public async Task ShouldFilterByNameSuccessfully(string nameQueryFilter, string[] expectedGameNames)
        {
            var query = new GetGamesWithPaginationQuery()
            {
                Name = nameQueryFilter,
            };

            var result = await SendQueryAsync(query);

            result.Items.Should().HaveCount(expectedGameNames.Length);
            result.Items.Select(i => i.Name).Should().BeEquivalentTo(expectedGameNames);
        }

        [Test]
        [TestCase(GameGenre.RPG, 2)]
        [TestCase(GameGenre.Action, 1)]
        [TestCase(GameGenre.Adventure, 3)]
        [TestCase(GameGenre.Sports, 0)]
        [TestCase(null, TotalNumberOfGames)]
        public async Task ShouldFilterByGenreSuccessfully(GameGenre? gameGenre, int expectedItemsCount)
        {
            var query = new GetGamesWithPaginationQuery()
            {
                Genre = gameGenre
            };

            var result = await SendQueryAsync(query);

            result.Items.Should().HaveCount(expectedItemsCount);
        }

        [Test]
        [TestCase("PS5", 3)]
        [TestCase("Xbox", 2)]
        [TestCase("Computer", 2)]
        [TestCase("Switch", 1)]
        [TestCase("", TotalNumberOfGames)]
        public async Task ShouldFilterByGamingPlatformIdSuccessfully(string gamingPlatformDictionaryName, int expectedItemsCount)
        {
            var gamingPlatformId = GamingPlatforms.GetValueOrDefault(gamingPlatformDictionaryName)?.Id;

            var query = new GetGamesWithPaginationQuery()
            {
                GamingPlatformId = gamingPlatformId,
            };

            var result = await SendQueryAsync(query);

            result.Items.Should().HaveCount(expectedItemsCount);
        }

        [Test]
        [TestCase("Bill", 3)]
        [TestCase("Mark", 1)]
        [TestCase("Steve", 0)]
        [TestCase("", TotalNumberOfGames)]
        public async Task ShouldFilterByFriendIdSuccessfully(string friendDictionaryName, int expectedItemsCount)
        {
            var friendId = Friends.GetValueOrDefault(friendDictionaryName)?.Id;

            var query = new GetGamesWithPaginationQuery()
            {
                FriendId = friendId,
            };

            var result = await SendQueryAsync(query);

            result.Items.Should().HaveCount(expectedItemsCount);
        }

        [Test]
        [TestCase("cra",     null,                "Xbox",   null,   2)]
        [TestCase("Zelda", null, "PS5", null, 0)]
        [TestCase("Zelda", null, "Switch", null, 1)]
        [TestCase("Zelda", GameGenre.RPG, "Switch", null, 0)]
        [TestCase("Zelda", GameGenre.Adventure, "Switch", null, 1)]
        [TestCase("", GameGenre.RPG, "PS5", null, 1)]
        [TestCase("", GameGenre.RPG, "PS5", "Mark", 0)]
        [TestCase("", GameGenre.Adventure, "PS5", "Mark", 1)]
        [TestCase("Detroit", GameGenre.Adventure, "PS5", "Mark", 1)]
        public async Task ShouldCombineFiltersAndFilterSuccessfully(string name, GameGenre? gameGenre, string gamingPlatformDictionaryName, string friendDictionaryName, int expectedItemsCount)
        {
            var gamingPlatformId = GamingPlatforms.GetValueOrDefault(gamingPlatformDictionaryName ?? string.Empty)?.Id;
            var friendId = Friends.GetValueOrDefault(friendDictionaryName ?? string.Empty)?.Id;

            var query = new GetGamesWithPaginationQuery()
            {
                Name = name,
                Genre = gameGenre,
                GamingPlatformId = gamingPlatformId,
                FriendId = friendId,
            };

            var result = await SendQueryAsync(query);

            result.Items.Should().HaveCount(expectedItemsCount);
        }

        [Test]
        public async Task ShouldReturnGamesOrderedByName()
        {
            var query = new GetGamesWithPaginationQuery();

            var result = await SendQueryAsync(query);

            result.Items.Select(i => i.Name).Should().BeInAscendingOrder();
        }

        [Test]
        public async Task ShouldIncludePlatformId()
        {
            var query = new GetGamesWithPaginationQuery();

            var result = await SendQueryAsync(query);

            var actualPlatforms = result.Items.Select(i => i.Platform.Id).ToList();
            var expectedPlatforms = result.Items.Select(i => GamesDatabaseSetup.Values.First(e => e.Id == i.Id).Platform.Id).ToList();

            actualPlatforms.Should().BeEquivalentTo(expectedPlatforms);
        }

        [Test]
        public async Task ShouldIncludeFriendId()
        {
            var query = new GetGamesWithPaginationQuery();

            var result = await SendQueryAsync(query);

            var actualFriends = result.Items.Select(i => i.LoanedTo?.Id).ToList();
            var expectedFriends = result.Items.Select(i => GamesDatabaseSetup.Values.First(e => e.Id == i.Id).LoanedTo?.Id).ToList();

            actualFriends.Should().BeEquivalentTo(expectedFriends);
        }

        [Test]
        public async Task ShouldReturnAllGames()
        {
            var query = new GetGamesWithPaginationQuery();

            var result = await SendQueryAsync(query);

            result.Items.Should().HaveCount(TotalNumberOfGames);
            result.Items.Select(i => i.Name).Should().BeEquivalentTo(GamesDatabaseSetup.Values.Select(g => g.Name));
        }
    }
}

