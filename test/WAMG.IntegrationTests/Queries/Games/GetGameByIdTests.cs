﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Application.Queries.Games.GetGameById;
using WAMG.Domain.Entities;
using WAMG.Domain.Enums;

namespace WAMG.IntegrationTests.Queries.Games
{
    using static FluentActions;
    using static TestingHelpers;

    public class GetGameByIdTests : TestBase
    {
        private readonly Game TestGame = new Game()
        {
            Id = 1,
            Name = "Demon's Souls",
            Genre = GameGenre.RPG,
            Platform = new GamingPlatform()
            {
                Id = 1,
                Name = "Playstation 5",
                IsConsole = true,
                IsHandheld = false,
                CreatedAt = DateTime.Now,
            },
            LoanedTo = new Friend()
            {
                Id = 1,
                Name = "Bill Gates",
                Email = "bill@gates.com",
                PhoneNumber = "+5527987654321",
                CreatedAt = DateTime.Now,
            },
            CreatedAt = DateTime.Now,
        };

        [SetUp]
        public async Task PrepareDatabaseWithGame()
        {
            await PrepareDatabaseWith(new[] { TestGame });
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var query = new GetGameByIdQuery(999);

            Invoking(() => SendCommandAsync(query))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity Game (999) was not found");
        }

        [Test]
        public async Task ShouldFindEntityByIdSuccessfully()
        {
            var query = new GetGameByIdQuery(TestGame.Id);

            var result = await SendQueryAsync(query);

            result.Id.Should().Be(TestGame.Id);
            result.Name.Should().Be(TestGame.Name);
            result.Genre.Should().Be(TestGame.Genre.ToString());
            result.Platform.Id.Should().Be(TestGame.Platform.Id);
            result.Platform.Name.Should().Be(TestGame.Platform.Name);
            result.Platform.IsConsole.Should().Be(TestGame.Platform.IsConsole);
            result.Platform.IsHandheld.Should().Be(TestGame.Platform.IsHandheld);
            result.LoanedTo.Id.Should().Be(TestGame.LoanedTo.Id);
            result.LoanedTo.Name.Should().Be(TestGame.LoanedTo.Name);
            result.LoanedTo.Email.Should().Be(TestGame.LoanedTo.Email);
            result.LoanedTo.PhoneNumber.Should().Be(TestGame.LoanedTo.PhoneNumber);
        }


    }
}
