﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Application.Queries.Games.GetGenres;
using WAMG.Domain.Enums;

namespace WAMG.IntegrationTests.Queries.Games
{
    using static TestingHelpers;

    public class GetGenresTests : TestBase
    {

        [Test]
        public async Task ShouldGetAllGenres()
        {
            var genres = Enum.GetValues<GameGenre>().Select(g => g.ToString()).ToList();

            var query = new GetGenresQuery();

            var result = await SendQueryAsync(query);

            result.Should().BeEquivalentTo(genres);
        }

    }
}
