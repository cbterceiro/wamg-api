﻿using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Respawn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Core.Model;
using WAMG.Infrastructure.Persistence;

namespace WAMG.IntegrationTests
{
    public static class TestingHelpers
    {
        private static IConfigurationRoot ConfigurationRoot;
        private static IServiceScopeFactory ScopeFactory;
        private static Checkpoint Checkpoint;

        public static void SetConfiguration(IConfigurationRoot configurationRoot)
        {
            ConfigurationRoot = configurationRoot;
        }

        public static void SetScopeFactory(IServiceScopeFactory scopeFactory)
        {
            ScopeFactory = scopeFactory;
        }

        public static void SetCheckpoint(Checkpoint checkpoint)
        {
            Checkpoint = checkpoint;
        }

        public static IConfigurationRoot Configuration => ConfigurationRoot;
        public static IServiceScope CreateScope() => ScopeFactory.CreateScope();
        public static T GetService<T>(IServiceScope scope) => scope.ServiceProvider.GetService<T>();

        public static async Task<T> SendCommandAsync<T>(IRequest<T> command)
        {
            using var scope = CreateScope();
            var mediator = GetService<IMediator>(scope);
            return await mediator.Send(command);
        }
        
        public static async Task<T> SendQueryAsync<T>(IRequest<T> query)
        {
            return await SendCommandAsync(query); // in this context, query and command are the same
        }

        public static async Task ResetState()
        {
            using var scope = CreateScope();
            var context = GetService<ApplicationDbContext>(scope);
            var conn = context.Database.GetDbConnection();
            conn.Open();
            await Checkpoint.Reset(conn);
        }

        public static async Task PrepareDatabaseWith(object[] entities)
        {
            using var scope = CreateScope();
            var context = GetService<ApplicationDbContext>(scope);
            foreach (var e in entities)
            {
                context.Add(e);
            }
            await context.SaveChangesAsync();
        }

        public static async Task<TEntity> CheckDatabaseHasOne<TEntity>(Expression<Func<TEntity,bool>> predicate = null) where TEntity : BaseEntity
        {
            using var scope = CreateScope();
            var context = GetService<ApplicationDbContext>(scope);
            var queryable = context.Set<TEntity>().AsQueryable();

            if (predicate != null)
            {
                queryable = queryable.Where(predicate);
            }

            var entitiesOnDatabase = await queryable.ToListAsync();

            entitiesOnDatabase.Should().HaveCount(1);

            return entitiesOnDatabase.First();
        }

        public static async Task CheckDatabaseHasZero<TEntity>(Expression<Func<TEntity, bool>> predicate = null) where TEntity : BaseEntity
        {
            using var scope = CreateScope();
            var context = GetService<ApplicationDbContext>(scope);
            var queryable = context.Set<TEntity>().AsQueryable();

            if (predicate != null)
            {
                queryable = queryable.Where(predicate);
            }

            var entitiesOnDatabase = await queryable.ToListAsync();

            entitiesOnDatabase.Should().HaveCount(0);
        }

        public static void SendCommandShouldThrowValidationExceptionWith<T>(IRequest<T> command, Dictionary<string, string[]> errors)
        {
            var assertions = FluentActions.Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<ValidationException>()
                .And.Errors.Should().HaveCount(errors.Count).And;

            foreach (var key in errors.Keys)
            {
                assertions.ContainKey(key).WhichValue.Should().BeEquivalentTo(errors[key]);
            }
        }
        public static void SendQueryShouldThrowValidationExceptionWith<T>(IRequest<T> command, Dictionary<string, string[]> errors)
        {
            SendCommandShouldThrowValidationExceptionWith(command, errors); // in this context, query and command are the same
        }

        public static Dictionary<string, string[]> ErrorDictionaryFrom(string key, string value)
        {
            return ErrorDictionaryFrom(key, new string[] { value });
        }

        public static Dictionary<string, string[]> ErrorDictionaryFrom(string key, string[] values)
        {
            return new Dictionary<string, string[]>()
            {
                { key, values },
            };
        }
    }
}
