﻿using FluentAssertions;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using NUnit.Framework;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAMG.Application.Commands.ApplicationUsers.Authenticate;
using WAMG.Infrastructure.Identity;

namespace WAMG.IntegrationTests.Commands.ApplicationUsers
{
    using static FluentActions;
    using static TestingHelpers;

    public class AuthenticateTests : TestBase
    {
        private const string DefaultUserUserName = "WAMG.Admin";
        private const string DefaultUserPassword = "A_strong_passw0rd!";
        private const string DefaultUserEmail = "admin@wamg.com";
        private const string DefaultUserRole = "Administrator";

        [SetUp]
        public async Task PrepareTestsWithDefaultUser()
        {
            using var scope = CreateScope();

            var userManager = GetService<UserManager<ApplicationUser>>(scope);
            var roleManager = GetService<RoleManager<ApplicationRole>>(scope);
            
            var administratorRole = new ApplicationRole(DefaultUserRole);
            var administrator = new ApplicationUser { UserName = DefaultUserUserName, Email = DefaultUserEmail };
            
            await roleManager.CreateAsync(administratorRole);
            await userManager.CreateAsync(administrator, DefaultUserPassword);
            await userManager.AddToRolesAsync(administrator, new[] { administratorRole.Name });
        }


        [Test]
        public void ShouldValidateEmptyUsername()
        {
            var command = new AuthenticateCommand()
            {
                Username = "",
                Password = "some password",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Username", "Please provide a username"));
        }

        [Test]
        public void ShouldValidateEmptyPassword()
        {
            var command = new AuthenticateCommand()
            {
                Username = "some username",
                Password = "",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Password", "Please provide a password"));
        }

        [Test]
        [TestCase(DefaultUserUserName, "with a wrong password")]
        [TestCase("The wrong username with", DefaultUserPassword)]
        [TestCase("A random username", "with a random password")]
        public void ShouldNotAuthenticateWrongUsernameOrPassword(string username, string password)
        {
            var command = new AuthenticateCommand()
            {
                Username = username,
                Password = password,
            };

            Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<UnauthorizedAccessException>()
                    .WithMessage("Wrong username or password");
        }

        [Test]
        public async Task ShouldAuthenticateSuccessfullyAndGenerateValidJwtToken()
        {
            var command = new AuthenticateCommand()
            {
                Username = DefaultUserUserName,
                Password = DefaultUserPassword
            };

            var result = await SendCommandAsync(command);

            var accessToken = result.AccessToken;

            var tokenValidator = new JwtSecurityTokenHandler();

            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["AuthenticationToken:Key"])),
                ValidateIssuerSigningKey = true,
                ValidateAudience = false,
                ValidateIssuer = false
            };

            var principal = tokenValidator.ValidateToken(accessToken, tokenValidationParameters, out _);

            principal.Claims.Select(c => c.Value).Should().Contain(command.Username);
        }
    }
}
