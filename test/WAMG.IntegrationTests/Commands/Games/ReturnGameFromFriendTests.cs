﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Application.Commands.Games.ReturnGameFromFriend;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;
using WAMG.Domain.Enums;

namespace WAMG.IntegrationTests.Commands.Games
{
    using static TestingHelpers;
    using static FluentActions;

    public class ReturnGameFromFriendTests : TestBase
    {
        private static readonly GamingPlatform GamingPlatformDatabaseSetup = new GamingPlatform() { Id = 1, Name = "Playstation 5", IsConsole = true, IsHandheld = false };
        private static readonly Friend FriendDatabaseSetup = new Friend() { Id = 1, Name = "Bill Gates", Email = "bill@gates.com", PhoneNumber = "+5527987654321" };

        private readonly Game TestGame = new Game()
        {
            Id = 1,
            Name = "Demon's Souls",
            Genre = GameGenre.RPG,
            PlatformId = GamingPlatformDatabaseSetup.Id,
            LoanedToId = FriendDatabaseSetup.Id,
        };

        [SetUp]
        public async Task PrepareDatabase()
        {
            await PrepareDatabaseWith(new object[]
            {
                GamingPlatformDatabaseSetup,
                FriendDatabaseSetup,
                TestGame,
            });
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var command = new ReturnGameFromFriendCommand(999);

            Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity Game (999) was not found");
        }

        [Test]
        public async Task ShouldReturnGameFromFriendSuccessfully()
        {
            var command = new ReturnGameFromFriendCommand(TestGame.Id);

            await SendCommandAsync(command);

            await CheckDatabaseHasOne<Game>(g =>
                g.Id == TestGame.Id && g.LoanedToId == null
            );
        }

        [Test]
        public async Task ShouldReturnGameFromFriendSuccessfullyWithoutChangingOtherFields()
        {
            var command = new ReturnGameFromFriendCommand(TestGame.Id);

            await SendCommandAsync(command);

            var entityOnDatabase = await CheckDatabaseHasOne<Game>(g =>
                g.Id == TestGame.Id && g.LoanedToId == null
            );

            entityOnDatabase.Name.Should().Be(TestGame.Name);
            entityOnDatabase.Genre.Should().Be(TestGame.Genre);
            entityOnDatabase.PlatformId.Should().Be(TestGame.PlatformId);
        }

    }
}
