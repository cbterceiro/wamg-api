﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Application.Commands.Games.LoanGameToFriend;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;
using WAMG.Domain.Enums;

namespace WAMG.IntegrationTests.Commands.Games
{
    using static FluentActions;
    using static TestingHelpers;

    public class LoanGameToFriendTests : TestBase
    {
        private static readonly GamingPlatform GamingPlatformDatabaseSetup = new GamingPlatform() { Id = 1, Name = "Playstation 5", IsConsole = true, IsHandheld = false };
        private static readonly Friend[] FriendDatabaseSetup = new Friend[]
        {
            new Friend() { Id = 1, Name = "Bill Gates", Email = "bill@gates.com", PhoneNumber = "+5527987654321" },
            new Friend() { Id = 2, Name = "Steve Jobs", Email = "steve@jobs.com", PhoneNumber = "+5527987654321" },
        };
        private readonly Game[] TestGames = new Game[]
        {
            new Game()
            {
                Id = 1,
                Name = "Demon's Souls",
                Genre = GameGenre.RPG,
                PlatformId = GamingPlatformDatabaseSetup.Id,
                LoanedToId = FriendDatabaseSetup[0].Id,
            },
            new Game()
            {
                Id = 2,
                Name = "Horizon II Forbidden West",
                Genre = GameGenre.Action,
                PlatformId = GamingPlatformDatabaseSetup.Id,
                LoanedToId = null,
            },
        };

        [SetUp]
        public async Task PrepareDatabase()
        {
            var entities = new List<object>();

            entities.Add(GamingPlatformDatabaseSetup);
            entities.AddRange(FriendDatabaseSetup);
            entities.AddRange(TestGames);

            await PrepareDatabaseWith(entities.ToArray());
        }

        [Test]
        public void ShouldValidateFriendExistOnDatabase()
        {
            var command = new LoanGameToFriendCommand()
            {
                GameId = 1,
                FriendId = 999,
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("FriendId", "The friend 999 was not found on the database"));
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenGameNotFound()
        {
            var command = new LoanGameToFriendCommand()
            {
                GameId = 999,
                FriendId = 1,
            };

            Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity Game (999) was not found");
        }

        [Test]
        [TestCase(1, 2)]
        [TestCase(2, 1)]
        [TestCase(2, 2)]
        public async Task ShouldUpdateGameLoanSuccessfully(int gameId, int friendId)
        {
            var command = new LoanGameToFriendCommand()
            {
                GameId = gameId,
                FriendId = friendId,
            };

            await SendCommandAsync(command);

            await CheckDatabaseHasOne<Game>(g =>
                g.Id == gameId && g.LoanedToId == friendId
            );
        }

        [Test]
        [TestCase(1, 2)]
        [TestCase(2, 1)]
        [TestCase(2, 2)]
        public async Task ShouldUpdateGameLoanSuccessfullyWithoutChangingOtherFields(int gameId, int friendId)
        {
            var command = new LoanGameToFriendCommand()
            {
                GameId = gameId,
                FriendId = friendId,
            };

            await SendCommandAsync(command);

            var entityOnDatabase = await CheckDatabaseHasOne<Game>(g =>
                g.Id == gameId && g.LoanedToId == friendId
            );

            var testEntity = TestGames.First(g => g.Id == gameId);

            entityOnDatabase.Name.Should().Be(testEntity.Name);
            entityOnDatabase.Genre.Should().Be(testEntity.Genre);
            entityOnDatabase.PlatformId.Should().Be(testEntity.PlatformId);
        }
    }
}
