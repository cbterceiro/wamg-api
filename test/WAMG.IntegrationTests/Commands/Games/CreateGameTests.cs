﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Commands.Games.CreateGame;
using WAMG.Domain.Entities;
using WAMG.Domain.Enums;

namespace WAMG.IntegrationTests.Commands.Games
{
    using static TestingHelpers;

    public class CreateGameTests : TestBase
    {
        private readonly GamingPlatform GamingPlatformDatabaseSetup = new GamingPlatform() { Id = 1, Name = "Playstation 5", IsConsole = true, IsHandheld = false };

        private CreateGameCommand ValidCreateGameCommand => new CreateGameCommand()
        {
            Name = "Some valid game name",
            Genre = GameGenre.Action.ToString(),
            PlatformId = 1
        };

        [SetUp]
        public async Task PrepareDatabase()
        {
            await PrepareDatabaseWith(new[] { GamingPlatformDatabaseSetup });
        }

        [Test]
        public void ShouldValidateGameNameNotEmpty()
        {
            var command = ValidCreateGameCommand;

            command.Name = "";

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The game's name must not be empty"));
        }

        [Test]
        public void ShouldValidateGameNameNotExceeding100Characters()
        {
            var command = ValidCreateGameCommand;

            command.Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded";

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The game's name must not exceed 100 characters"));
        }

        [Test]
        public void ShouldValidateGameGenreNotEmpty()
        {
            var command = ValidCreateGameCommand;

            command.Genre = "";

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Genre", "The game's genre must not be empty"));
        }

        [Test]
        public void ShouldValidateGameGenreIsValidEnum()
        {
            var command = ValidCreateGameCommand;

            command.Genre = "An invalid game genre";

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Genre", "The game's genre was not recognized"));
        }

        [Test]
        public void ShouldValidatePlatformIdGreaterThanOrEqualToOne()
        {
            var command = ValidCreateGameCommand;

            command.PlatformId = 0;

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("PlatformId", "The gaming platform identifier must be greater than or equal to 1"));
        }

        [Test]
        public void ShouldValidatePlatformExistOnDatabase()
        {
            var command = ValidCreateGameCommand;

            command.PlatformId = 999;

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("PlatformId", "The gaming platform 999 was not found on the database"));
        }

        [Test]
        public async Task ShouldCreateGameSuccessfully()
        {
            var command = ValidCreateGameCommand;

            var result = await SendCommandAsync(command);

            await CheckDatabaseHasOne<Game>(g =>
                g.Name.Equals(command.Name)
                && g.Genre.Equals(Enum.Parse<GameGenre>(command.Genre))
                && g.PlatformId.Equals(command.PlatformId)
            );
        }
    }
}
