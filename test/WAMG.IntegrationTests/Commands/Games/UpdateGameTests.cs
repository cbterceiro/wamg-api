﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Commands.Games.UpdateGame;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;
using WAMG.Domain.Enums;

namespace WAMG.IntegrationTests.Commands.Games
{
    using static FluentActions;
    using static TestingHelpers;
    public class UpdateGameTests : TestBase
    {
        private static readonly GamingPlatform GamingPlatformDatabaseSetup = new GamingPlatform() { Id = 1, Name = "Playstation 5", IsConsole = true, IsHandheld = false };
        private static readonly Friend FriendDatabaseSetup = new Friend() { Id = 1, Name = "Bill Gates", Email = "bill@gates.com", PhoneNumber = "+5527987654321" };
        
        private readonly Game TestGame = new Game()
        { 
            Id = 1,
            Name = "Demon's Souls",
            Genre = GameGenre.RPG,
            PlatformId = GamingPlatformDatabaseSetup.Id,
            LoanedToId = FriendDatabaseSetup.Id,
        };

        private UpdateGameCommand ValidUpdateGameCommand => new UpdateGameCommand()
        {
            Id = 1,
            Name = "Some valid game name",
            Genre = GameGenre.Action.ToString(),
            PlatformId = 1,
        };

        [SetUp]
        public async Task PrepareDatabase()
        {
            await PrepareDatabaseWith(new object[]
            {
                GamingPlatformDatabaseSetup,
                FriendDatabaseSetup,
                TestGame,
            });
        }

        [Test]
        public void ShouldValidateGameNameNotEmpty()
        {
            var command = ValidUpdateGameCommand;

            command.Name = "";

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The game's name must not be empty"));
        }

        [Test]
        public void ShouldValidateGameNameNotExceeding100Characters()
        {
            var command = ValidUpdateGameCommand;

            command.Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded";

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The game's name must not exceed 100 characters"));
        }

        [Test]
        public void ShouldValidateGameGenreNotEmpty()
        {
            var command = ValidUpdateGameCommand;

            command.Genre = "";

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Genre", "The game's genre must not be empty"));
        }

        [Test]
        public void ShouldValidateGameGenreIsValidEnum()
        {
            var command = ValidUpdateGameCommand;

            command.Genre = "An invalid game genre";

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Genre", "The game's genre was not recognized"));
        }

        [Test]
        public void ShouldValidatePlatformIdGreaterThanOrEqualToOne()
        {
            var command = ValidUpdateGameCommand;

            command.PlatformId = 0;

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("PlatformId", "The gaming platform identifier must be greater than or equal to 1"));
        }

        [Test]
        public void ShouldValidatePlatformExistOnDatabase()
        {
            var command = ValidUpdateGameCommand;

            command.PlatformId = 999;

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("PlatformId", "The gaming platform 999 was not found on the database"));
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var command = ValidUpdateGameCommand;

            command.Id = 999;

            Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity Game (999) was not found");
        }

        [Test]
        public async Task ShouldUpdateGameSuccessfully()
        {
            var command = ValidUpdateGameCommand;

            var result = await SendCommandAsync(command);

            var entityOnDatabase = await CheckDatabaseHasOne<Game>(g =>
                g.Name.Equals(command.Name)
                && g.Genre.Equals(Enum.Parse<GameGenre>(command.Genre))
                && g.PlatformId.Equals(command.PlatformId)
            );

            entityOnDatabase.LastUpdatedAt.Should().BeCloseTo(DateTime.Now, 1000);
        }

        [Test]
        public async Task ShouldUpdateGameSuccessfullyWithoutChangingFriendLoan()
        {
            var command = ValidUpdateGameCommand;

            var result = await SendCommandAsync(command);

            var entityOnDatabase = await CheckDatabaseHasOne<Game>(g =>
                g.Name.Equals(command.Name)
                && g.Genre.Equals(Enum.Parse<GameGenre>(command.Genre))
                && g.PlatformId.Equals(command.PlatformId)

                && g.LoanedToId.Equals(TestGame.LoanedToId)
            );

            entityOnDatabase.LastUpdatedAt.Should().BeCloseTo(DateTime.Now, 1000);
        }
    }
}
