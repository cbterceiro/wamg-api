﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Commands.Friends.UpdateFriend;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Commands.Friends
{
    using static FluentActions;
    using static TestingHelpers;

    public class UpdateFriendTests : TestBase
    {
        private readonly Friend TestFriend = new Friend()
        {
            Id = 1,
            Name = "Test friend",
            Email = "test@friends.com",
            PhoneNumber = "+5527987654321",
            CreatedAt = DateTime.Now
        };

        [SetUp]
        public async Task PrepareDatabaseWithFriend()
        {
            await PrepareDatabaseWith(new[]
            {
                TestFriend,
            });
        }        
        
        [Test]
        public void ShouldValidateFriendNameNotEmpty()
        {
            var command = new UpdateFriendCommand()
            {
                Name = "",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The friend's name must not be empty"));
        }

        [Test]
        public void ShouldValidateFriendNameNotExceeding100Characters()
        {
            var command = new UpdateFriendCommand()
            {
                Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The friend's name must not exceed 100 characters"));
        }

        [Test]
        public void ShouldValidateFriendEmailBeingAValidEmailAddress()
        {
            var command = new UpdateFriendCommand()
            {
                Name = "Some valid name",
                Email = "not a valid email address",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Email", "The friend's email address must be a valid one"));
        }

        [Test]
        public void ShouldValidateFriendEmailNotExceeding50Characters()
        {
            var command = new UpdateFriendCommand()
            {
                Name = "Some valid name",
                Email = "validButLoooooooooooooooongEmailAddress@somecompany.com",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Email", "The friend's email address must not exceed 50 characters"));
        }

        [Test]
        public void ShouldValidatePhoneNumberNotExceeding20Characters()
        {
            var command = new UpdateFriendCommand()
            {
                Name = "Some valid name",
                PhoneNumber = "12345678901234567890999999",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("PhoneNumber", "The friend's phone number must not exceed 20 characters"));
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var command = new UpdateFriendCommand()
            {
                Id = 999,
                Name = "Some valid name"
            };

            Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity Friend (999) was not found");
        }

        [Test]
        [TestCase("A new name", null, null)]
        [TestCase("A new name", "newemail@tests.com", null)]
        [TestCase("A new name", null, "+5527123456789")]
        public async Task ShouldUpdateFriendSuccessfully(string name, string email, string phoneNumber)
        {
            var command = new UpdateFriendCommand()
            {
                Id = TestFriend.Id,
                Name = name,
                Email = email,
                PhoneNumber = phoneNumber,
            };

            await SendCommandAsync(command);

            var entityOnDatabase = await CheckDatabaseHasOne<Friend>(f =>
                string.Equals(f.Name, name)
                && string.Equals(f.Email, email)
                && string.Equals(f.PhoneNumber, phoneNumber)
            );
            entityOnDatabase.LastUpdatedAt.Should().BeCloseTo(DateTime.Now, 1000);
        }

    }
}
