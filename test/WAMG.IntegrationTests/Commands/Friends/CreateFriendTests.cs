﻿using NUnit.Framework;
using System.Threading.Tasks;
using WAMG.Application.Commands.Friends.CreateFriend;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Commands.Friends
{
    using static TestingHelpers;

    public class CreateFriendTests : TestBase
    {
        
        [Test]
        public void ShouldValidateFriendNameNotEmpty()
        {
            var command = new CreateFriendCommand()
            {
                Name = "",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The friend's name must not be empty"));
        }

        [Test]
        public void ShouldValidateFriendNameNotExceeding100Characters()
        {
            var command = new CreateFriendCommand()
            {
                Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The friend's name must not exceed 100 characters"));
        }

        [Test]
        public void ShouldValidateFriendEmailBeingAValidEmailAddress()
        {
            var command = new CreateFriendCommand()
            {
                Name = "Some valid name",
                Email = "not a valid email address",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Email", "The friend's email address must be a valid one"));
        }

        [Test]
        public void ShouldValidateFriendEmailNotExceeding50Characters()
        {
            var command = new CreateFriendCommand()
            {
                Name = "Some valid name",
                Email = "validButLoooooooooooooooongEmailAddress@somecompany.com",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Email", "The friend's email address must not exceed 50 characters"));
        }

        [Test]
        public void ShouldValidatePhoneNumberNotExceeding20Characters()
        {
            var command = new CreateFriendCommand()
            {
                Name = "Some valid name",
                PhoneNumber = "12345678901234567890999999",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("PhoneNumber", "The friend's phone number must not exceed 20 characters"));
        }

        [Test]
        [TestCase("Friend without email and phone", null, null)]
        [TestCase("Friend without phone", "valid@email.com", null)]
        [TestCase("Friend without email", null, "+5527987654321")]
        [TestCase("Friend with complete data", "valid@email.com", "+5527987654321")]
        public async Task ShouldCreateFriendSuccessfully(string name, string email, string phoneNumber)
        {
            var command = new CreateFriendCommand()
            {
                Name = name,
                Email = email,
                PhoneNumber = phoneNumber,
            };

            await SendCommandAsync(command);

            await CheckDatabaseHasOne<Friend>(f =>
                f.Name.Equals(name)
                && f.Email.Equals(email)
                && f.PhoneNumber.Equals(phoneNumber)
            );
        }

    }
}
