﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Commands.Friends.RemoveFriend;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Commands.Friends
{
    using static TestingHelpers;
    using static FluentActions;

    public class RemoveFriendTests : TestBase
    {
        private readonly Friend TestFriend = new Friend()
        {
            Id = 1,
            Name = "Test friend",
            Email = "test@friends.com",
            PhoneNumber = "+5527987654321",
            CreatedAt = DateTime.Now
        };

        [SetUp]
        public async Task PrepareDatabaseWithFriend()
        {
            await PrepareDatabaseWith(new[]
            {
                TestFriend,
            });
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var command = new RemoveFriendCommand(999);

            Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity Friend (999) was not found");
        }

        [Test]
        public async Task ShouldRemoveFriendSuccessfully()
        {
            var command = new RemoveFriendCommand(TestFriend.Id);

            await SendCommandAsync(command);

            await CheckDatabaseHasZero<Friend>();
        }
    }
}
