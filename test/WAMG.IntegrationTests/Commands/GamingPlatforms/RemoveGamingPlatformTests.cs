﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Commands.GamingPlatforms.RemoveGamingPlatform;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Commands.GamingPlatforms
{
    using static TestingHelpers;
    using static FluentActions;

    public class RemoveGamingPlatformTests : TestBase
    {
        private readonly GamingPlatform TestGamingPlatform = new GamingPlatform()
        {
            Id = 1,
            Name = "Test gaming platform",
            IsConsole = true,
            IsHandheld = false,
            CreatedAt = DateTime.Now
        };

        [SetUp]
        public async Task PrepareDatabaseWithGamingPlatform()
        {
            await PrepareDatabaseWith(new[]
            {
                TestGamingPlatform,
            });
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var command = new RemoveGamingPlatformCommand(999);

            Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity GamingPlatform (999) was not found");
        }

        [Test]
        public async Task ShouldRemoveGamingPlatformSuccessfully()
        {
            var command = new RemoveGamingPlatformCommand(TestGamingPlatform.Id);

            await SendCommandAsync(command);

            await CheckDatabaseHasZero<GamingPlatform>();
        }
    }
}
