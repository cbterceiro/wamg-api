﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Commands.GamingPlatforms.UpdateGamingPlatform;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Commands.GamingPlatforms
{
    using static FluentActions;
    using static TestingHelpers;

    public class UpdateGamingPlatformTests : TestBase
    {
        private readonly GamingPlatform TestGamingPlatform = new GamingPlatform()
        {
            Id = 1,
            Name = "Test gaming platform",
            IsConsole = true,
            IsHandheld = false,
            CreatedAt = DateTime.Now
        };

        [SetUp]
        public async Task PrepareDatabaseWithGamingPlatform()
        {
            await PrepareDatabaseWith(new[] { TestGamingPlatform });
        }   
        
        [Test]
        public void ShouldValidateGamingPlatformNameNotEmpty()
        {
            var command = new UpdateGamingPlatformCommand()
            {
                Id = TestGamingPlatform.Id,
                Name = "",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The gaming platform's name must not be empty"));
        }

        [Test]
        public void ShouldValidateGamingPlatformNameNotExceeding100Characters()
        {
            var command = new UpdateGamingPlatformCommand()
            {
                Id = TestGamingPlatform.Id,
                Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The gaming platform's name must not exceed 100 characters"));
        }

        [Test]
        public async Task ShouldValidateGamingPlatformNameIsUnique()
        {
            await PrepareDatabaseWith(new[]
            {
                new GamingPlatform() { Name = "Yet another gaming platform", IsConsole = false, IsHandheld = false },
            });

            var command = new UpdateGamingPlatformCommand()
            {
                Id = TestGamingPlatform.Id,
                Name = "Yet another gaming platform",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "There is already another gaming platform with this name"));
        }

        [Test]
        public void ShouldThrowEntityNotFoundExceptionWhenNotFound()
        {
            var command = new UpdateGamingPlatformCommand()
            {
                Id = 999,
                Name = "Some valid name"
            };

            Invoking(() => SendCommandAsync(command))
                .Should()
                .Throw<EntityNotFoundException>()
                .WithMessage("Entity GamingPlatform (999) was not found");
        }

        [Test]
        [TestCase("A new name", false, true)]
        [TestCase("A new name", true, true)]
        [TestCase("A new name", false, false)]
        public async Task ShouldUpdateGamingPlatformSuccessfully(string name, bool isConsole, bool isHandheld)
        {
            var command = new UpdateGamingPlatformCommand()
            {
                Id = TestGamingPlatform.Id,
                Name = name,
                IsConsole = isConsole,
                IsHandheld = isHandheld,
            };

            await SendCommandAsync(command);

            var entityOnDatabase = await CheckDatabaseHasOne<GamingPlatform>(f =>
                string.Equals(f.Name, name)
                && string.Equals(f.IsConsole, isConsole)
                && string.Equals(f.IsHandheld, isHandheld)
            );
            entityOnDatabase.LastUpdatedAt.Should().BeCloseTo(DateTime.Now, 1000);
        }

    }
}
