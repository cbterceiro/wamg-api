﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WAMG.Application.Commands.GamingPlatforms.CreateGamingPlatform;
using WAMG.Domain.Entities;

namespace WAMG.IntegrationTests.Commands.GamingPlatforms
{
    using static TestingHelpers;

    public class CreateGamingPlatformTests : TestBase
    {
        private readonly GamingPlatform TestGamingPlatform = new GamingPlatform()
        {
            Id = 1,
            Name = "Test gaming platform",
            IsConsole = true,
            IsHandheld = false,
            CreatedAt = DateTime.Now
        };

        [SetUp]
        public async Task PrepareDatabaseWithGamingPlatform()
        {
            await PrepareDatabaseWith(new[] { TestGamingPlatform });
        }

        [Test]
        public void ShouldValidateGamingPlatformNameNotEmpty()
        {
            var command = new CreateGamingPlatformCommand()
            {
                Name = "",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The gaming platform's name must not be empty"));
        }

        [Test]
        public void ShouldValidateGamingPlatformNameNotExceeding100Characters()
        {
            var command = new CreateGamingPlatformCommand()
            {
                Name = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 Exceeded",
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "The gaming platform's name must not exceed 100 characters"));
        }

        [Test]
        public void ShouldValidateGamingPlatformNameIsUnique()
        {
            var command = new CreateGamingPlatformCommand()
            {
                Name = TestGamingPlatform.Name,
            };

            SendCommandShouldThrowValidationExceptionWith(command, ErrorDictionaryFrom("Name", "There is already another gaming platform with this name"));
        }

        [Test]
        [TestCase("Neither console nor handheld", false, false)]
        [TestCase("Handheld generic gaming platform", false, true)]
        [TestCase("Table console", true, false)]
        [TestCase("Handheld console", true, true)]
        public async Task ShouldCreateGamingPlatformSuccessfully(string name, bool isConsole, bool isHandheld)
        {
            var command = new CreateGamingPlatformCommand()
            {
                Name = name,
                IsConsole = isConsole,
                IsHandheld = isHandheld,
            };

            await SendCommandAsync(command);

            await CheckDatabaseHasOne<GamingPlatform>(f =>
                f.Name.Equals(name)
                && f.IsConsole.Equals(isConsole)
                && f.IsHandheld.Equals(isHandheld)
            );
        }

    }
}
