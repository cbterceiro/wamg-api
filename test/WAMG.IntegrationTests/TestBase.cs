﻿using NUnit.Framework;
using System.Threading.Tasks;

namespace WAMG.IntegrationTests
{
    public class TestBase
    {
        [SetUp]
        public Task TestSetUp()
        {
            return TestingHelpers.ResetState();
        }
    }
}
