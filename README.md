# WAMG API

**"Where are my games?"** If you are constantly running into this problem where you loan your games to your friends and keep losing track of them, then say no more fam, I got ya!

This is an Web API built on REST endpoints that lets you control who are your friends, what are the gaming platforms you own and, of course, where are your games.

## Setting the environment
To run this project, you'll need:
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [.NET 5.0 SDK](https://docs.docker.com/compose/install/)

## Running the application
1. Clone the project's code on your machine
2. Run `docker-compose up -d`
3. On your browser, open `http://localhost:5000/swagger` to see the available endpoints

## Stopping the application
1. Run `docker-compose down --rmi all` to stop the application and erase all generated Docker images

## Running the application on Visual Studio
The same result above can be reached by opening the solution and using the Play button with the `Docker Compose` configuration. It will automatically build and run both the database and API containers.

## Testing the endpoints
From the Swagger UI, you can test the API by providing a JWT Token on the "Authorize" modal. To get a valid token, scroll down and obtain one with the `POST /api/User/Authenticate` endpoint by providing the correct credentials, which are:
```json
{
  "username": "WAMG.Tester",
  "password": "A_gre4t_passw@rd!"
}
```
After getting the JWT access token from the authentication endpoint's response, copy and paste it on the "Authorize" modal following the template `Bearer <your_token_here>`. Example: "Bearer ey3TWa2ms..."

With the appropriate JWT token you can now test all the other endpoints.

## Integration tests

To run the integration tests located on `tests` folder, please first make sure that the database container is up and running. If it isn't, run `docker-compose up -d wamg.db`. The database used on the integration tests is separated from the actual application's database so all testing data don't mess with the developer's data.

After checking the database status, open Visual Studio, right-click the test project and select `Run Tests`. Hopefully all tests should run successfully!

## Seed data
While testing the API endpoints, you'll notice that only the GET gaming platform endpoints will have some seed data already on the database. To properly test the friends and games endpoints, please POST the entities first.

## Summary of technologies/dependencies used
Some of the main NuGet packages used on the main application are:
- .NET 5.0 with ASP.NET 5.0 Runtime
- MediatR 9.0.0
- FluentValidation 9.3.0
- AutoMapper 10.1.1
- Entity Framework Core 5.0.0 (with Code-First Migrations)
- AspNetCore.Identity with EFCore 5.0.0
- Swashbuckle (Swagger) for .NET Core 5.6.3

On the test layer, the main dependencies are:
- NUnit 3.12.0
- Moq 4.15.2
- FluentAssertions 5.10.3
- Respawn 3.3.0

## Architecture
Heavily inspired by the Onion Architecture, this project tries to encapsulate some of the best practices and current software patterns.

- Clean/Onion Architecture
- S.O.L.I.D principles
- Clean Code
- Native DI
- Mediator Pattern
- CQRS Pattern
- Repository Pattern
- Query Specification Pattern
- Unit of Work Pattern
- JWT-based Authentication
- Authentication/Authorization with .NET Core Identity