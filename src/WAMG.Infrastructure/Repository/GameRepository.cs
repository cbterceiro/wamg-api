﻿using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;
using WAMG.Infrastructure.Persistence.Common;

namespace WAMG.Infrastructure.Repository
{
    public class GameRepository : BaseRepository<Game>, IGameRepository
    {
        public GameRepository(IQueryRepository<Game> queryRepository, IMutationRepository<Game> mutationRepository) : base(queryRepository, mutationRepository)
        {
        }
    }
}
