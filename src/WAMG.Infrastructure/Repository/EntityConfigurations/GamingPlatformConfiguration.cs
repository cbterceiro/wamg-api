﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WAMG.Domain.Entities;

namespace WAMG.Infrastructure.Repository.EntityConfigurations
{
    public class GamingPlatformConfiguration : IEntityTypeConfiguration<GamingPlatform>
    {
        public void Configure(EntityTypeBuilder<GamingPlatform> builder)
        {
            builder.HasKey(g => g.Id);

            builder.Property(g => g.Name)
                .IsRequired()
                .HasMaxLength(100);
            builder.HasIndex(g => g.Name).IsUnique();

            builder.Property(g => g.IsConsole)
                .IsRequired()
                .HasDefaultValue(false);

            builder.Property(g => g.IsHandheld)
                .IsRequired()
                .HasDefaultValue(false);
        }
    }
}
