﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;
using WAMG.Domain.Entities;
using WAMG.Domain.Enums;

namespace WAMG.Infrastructure.Repository.EntityConfigurations
{
    public class GameConfiguration : IEntityTypeConfiguration<Game>
    {
        public void Configure(EntityTypeBuilder<Game> builder)
        {
            builder.HasKey(g => g.Id);

            builder.Property(g => g.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(g => g.Genre)
                .IsRequired()
                .HasMaxLength(30)
                .HasConversion<string>();

            var genres = string.Join(", ", Enum.GetValues<GameGenre>().Select(g => "'" + g + "'").ToList());

            builder
                .HasCheckConstraint("CHECK_GameGenre_Enum", $"`genre` in ({genres})");

            builder
                .HasOne(g => g.Platform)
                .WithMany()
                .IsRequired()
                .HasForeignKey(g => g.PlatformId);

            builder
                .HasOne(g => g.LoanedTo)
                .WithMany()
                .HasForeignKey(g => g.LoanedToId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
