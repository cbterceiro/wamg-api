﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WAMG.Domain.Entities;

namespace WAMG.Infrastructure.Repository.EntityConfigurations
{
    public class FriendConfiguration : IEntityTypeConfiguration<Friend>
    {
        public void Configure(EntityTypeBuilder<Friend> builder)
        {
            builder.HasKey(f => f.Id);

            builder.Property(f => f.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(f => f.Email)
                .HasMaxLength(50);

            builder.Property(f => f.PhoneNumber)
                .HasMaxLength(20);
        }
    }
}
