﻿using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;
using WAMG.Infrastructure.Persistence.Common;

namespace WAMG.Infrastructure.Repository
{
    public class GamingPlatformRepository : BaseRepository<GamingPlatform>, IGamingPlatformRepository
    {
        public GamingPlatformRepository(IQueryRepository<GamingPlatform> queryRepository, IMutationRepository<GamingPlatform> mutationRepository) : base(queryRepository, mutationRepository)
        {
        }
    }
}
