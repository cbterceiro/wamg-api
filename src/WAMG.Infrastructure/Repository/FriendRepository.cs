﻿using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;
using WAMG.Infrastructure.Persistence.Common;

namespace WAMG.Infrastructure.Repository
{
    public class FriendRepository : BaseRepository<Friend>, IFriendRepository
    {
        public FriendRepository(IQueryRepository<Friend> queryRepository, IMutationRepository<Friend> mutationRepository) : base(queryRepository, mutationRepository)
        {
        }
    }
}
