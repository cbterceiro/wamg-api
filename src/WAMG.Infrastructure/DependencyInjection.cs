﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WAMG.Core.Repository;
using WAMG.Domain.Repository;
using WAMG.Infrastructure.Repository;
using System;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Microsoft.AspNetCore.Builder;
using WAMG.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using WAMG.Application.Common.Identity;
using WAMG.Infrastructure.Persistence;
using WAMG.Infrastructure.Persistence.Common;

namespace WAMG.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            AddIdentity(services, configuration);
            AddDbContext(services, configuration);
            AddUnitOfWork(services);
            AddDomainRepositories(services);

            return services;
        }

        private static void AddIdentity(IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentityCore<ApplicationUser>()
               .AddRoles<ApplicationRole>()
               .AddEntityFrameworkStores<ApplicationIdentityDbContext>()
               .AddDefaultTokenProviders();

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["AuthenticationToken:Key"])),
                        ClockSkew = TimeSpan.Zero,
                    }
                );

            services.AddTransient<IIdentityService, IdentityService>();
        }

        private static void AddDbContext(IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            {
                AddInMemoryDatabase<ApplicationDbContext>(services);
                AddInMemoryDatabase<ApplicationIdentityDbContext>(services);
            }
            else
            {
                AddMySqlDatabase<ApplicationDbContext>(services, configuration.GetConnectionString("DefaultConnection"));
                AddMySqlDatabase<ApplicationIdentityDbContext>(services, configuration.GetConnectionString("DefaultConnection"));
            }
        }

        private static void AddInMemoryDatabase<TContext>(IServiceCollection services) where TContext : DbContext
        {
            services.AddDbContext<TContext>(options => options.UseInMemoryDatabase("wamg-db"));
        }

        private static void AddMySqlDatabase<TContext>(IServiceCollection services, string connectionString) where TContext : DbContext
        {
            services.AddDbContextPool<TContext>(options =>
                options.UseMySql(
                    connectionString,
                    new MySqlServerVersion(new Version(8, 0, 20)),
                    mySqlOptions => mySqlOptions.CharSetBehavior(CharSetBehavior.NeverAppend)
                )
            );
        }

        private static void AddUnitOfWork(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        private static void AddDomainRepositories(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(IQueryRepository<>), typeof(BaseQueryRepository<>));
            services.AddScoped(typeof(IMutationRepository<>), typeof(BaseMutationRepository<>));

            services.AddScoped<IFriendRepository, FriendRepository>();
            services.AddScoped<IGamingPlatformRepository, GamingPlatformRepository>();
            services.AddScoped<IGameRepository, GameRepository>();
        }
    }
}
