﻿using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Infrastructure.Identity;

namespace WAMG.Infrastructure.Persistence
{
    public static class ApplicationIdentityDbContextSeed
    {
        public static async Task SeedDefaultUsersAsync(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            await SeedAdministrator(userManager, roleManager);
            await SeedTester(userManager, roleManager);
        }

        private static async Task SeedAdministrator(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            var administratorRole = new ApplicationRole("Administrator");

            if (roleManager.Roles.All(r => r.Name != administratorRole.Name))
            {
                await roleManager.CreateAsync(administratorRole);
            }

            var administrator = new ApplicationUser { UserName = "WAMG.Admin", Email = "admin@wamg.com" };

            if (userManager.Users.All(u => u.UserName != administrator.UserName))
            {
                await userManager.CreateAsync(administrator, "A_strong_passw0rd!");
                await userManager.AddToRolesAsync(administrator, new[] { administratorRole.Name });
            }
        }

        private static async Task SeedTester(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            var testerRole = new ApplicationRole("Tester");

            if (roleManager.Roles.All(r => r.Name != testerRole.Name))
            {
                await roleManager.CreateAsync(testerRole);
            }

            var tester = new ApplicationUser { UserName = "WAMG.Tester", Email = "tester@wamg.com" };

            if (userManager.Users.All(u => u.UserName != tester.UserName))
            {
                await userManager.CreateAsync(tester, "A_gre4t_passw@rd!");
                await userManager.AddToRolesAsync(tester, new[] { testerRole.Name });
            }

        }
    }
}
