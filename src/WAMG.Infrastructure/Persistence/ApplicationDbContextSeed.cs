﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Domain.Entities;

namespace WAMG.Infrastructure.Persistence
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedGamingPlatforms(ApplicationDbContext context)
        {
            var gamingPlatforms = new GamingPlatform[]
            {
                new GamingPlatform()
                {
                    Name = "Playstation 5",
                    IsConsole = true,
                    IsHandheld = false,
                    CreatedAt = DateTime.Now,
                },
                new GamingPlatform()
                {
                    Name = "Xbox Series X",
                    IsConsole = true,
                    IsHandheld = false,
                    CreatedAt = DateTime.Now,
                },
                new GamingPlatform()
                {
                    Name = "Nintendo Switch",
                    IsConsole = true,
                    IsHandheld = true,
                    CreatedAt = DateTime.Now,
                },
                new GamingPlatform()
                {
                    Name = "Computer",
                    IsConsole = false,
                    IsHandheld = false,
                    CreatedAt = DateTime.Now,
                },
            };

            var thereAreGamingPlatformsAlreadyOnDatabase = await context.Set<GamingPlatform>().AnyAsync();

            if (!thereAreGamingPlatformsAlreadyOnDatabase)
            {
                await context.AddRangeAsync(gamingPlatforms);

                await context.SaveChangesAsync();
            }
        }
    }
}
