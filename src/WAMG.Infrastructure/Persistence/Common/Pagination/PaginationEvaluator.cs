﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Core.Model;
using WAMG.Core.Repository.Pagination;

namespace WAMG.Infrastructure.Persistence.Common.Pagination
{
    public static class PaginationEvaluator<T> where T : BaseEntity
    {
        public static async Task<IPaginationResult<T>> ApplyPaging(IQueryable<T> query, IPaginationQuery paginationQuery)
        {
            var pageNumber = paginationQuery.PageNumber;
            var pageSize = paginationQuery.PageSize;

            var totalCount = await query.CountAsync();
            var totalPages = (int)Math.Ceiling(totalCount / (double)(pageSize == 0 ? 1 : pageSize));

            query = query
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var items = await query.ToListAsync();

            return new BasePaginationResult<T>()
            {
                PageNumber = pageNumber,
                PageSize = pageSize,
                Items = items,
                TotalCount = totalCount,
                TotalPages = totalPages,
                HasPreviousPage = pageNumber > 1,
                HasNextPage = pageNumber < totalPages,
            };
        }
    }
}
