﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using WAMG.Core.Model;
using WAMG.Core.Repository;
using WAMG.Infrastructure.Persistence;

namespace WAMG.Infrastructure.Persistence.Common
{
    public class BaseMutationRepository<T> : IMutationRepository<T> where T : BaseEntity
    {
        protected readonly ApplicationDbContext _context;
        protected readonly DbSet<T> _dbSet;

        public BaseMutationRepository(ApplicationDbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public async Task Add(T entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task AddRange(IEnumerable<T> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }

        public Task Update(T entity)
        {
            _dbSet.Update(entity);
            return Task.CompletedTask;
        }

        public Task UpdateRange(IEnumerable<T> entity)
        {
            _dbSet.UpdateRange(entity);
            return Task.CompletedTask;
        }

        public Task Remove(T entity)
        {
            _dbSet.Remove(entity);
            return Task.CompletedTask;
        }

        public async Task Remove(int id)
        {
            _dbSet.Remove(await _dbSet.FindAsync(id));
        }

        public Task RemoveRange(IEnumerable<T> entities)
        {
            _dbSet.RemoveRange(entities);
            return Task.CompletedTask;
        }
    }
}
