﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using WAMG.Core.Model;
using WAMG.Core.Repository.Specification;

namespace WAMG.Infrastructure.Persistence.Common.Specification
{
    public static class QuerySpecificationEvaluator<T> where T : BaseEntity
    {
        public static IQueryable<T> ApplySpecification(IQueryable<T> query, IQuerySpecification<T> spec)
        {
            if (spec != null)
            {
                query = ApplyFilters(query, spec.FilterExpressions);
                query = ApplyOrdering(query, spec.OrderByExpressions);
                query = ApplyIncludes(query, spec.IncludeExpressions, spec.IncludeStrings);
            }
            return query;
        }

        private static IQueryable<T> ApplyFilters(IQueryable<T> query, IList<Expression<Func<T, bool>>> expressions)
        {
            if (expressions != null)
            {
                foreach (var expression in expressions)
                {
                    query = query.Where(expression);
                };
            }
            return query;
        }

        private static IQueryable<T> ApplyOrdering(IQueryable<T> query, IList<IOrderByExpression<T>> expressions)
        {
            if (expressions != null)
            {
                var firstOrderByExpression = expressions.FirstOrDefault();
                var remainingOrderByExpressions = expressions.Skip(1).ToList();

                if (firstOrderByExpression != null)
                {
                    query = firstOrderByExpression.Direction == OrderByExpressionDirection.Ascending
                        ? query.OrderBy(firstOrderByExpression.Expression)
                        : query.OrderByDescending(firstOrderByExpression.Expression);
                }

                if (remainingOrderByExpressions.Any())
                {
                    foreach (var orderExpression in remainingOrderByExpressions)
                    {
                        query = orderExpression.Direction == OrderByExpressionDirection.Ascending
                            ? ((IOrderedQueryable<T>)query).ThenBy(firstOrderByExpression.Expression)
                            : ((IOrderedQueryable<T>)query).ThenByDescending(firstOrderByExpression.Expression);
                    }
                }
            }
            return query;
        }

        private static IQueryable<T> ApplyIncludes(IQueryable<T> query, IList<Expression<Func<T, object>>> includeExpressions, IList<string> includeStrings)
        {
            if (includeExpressions != null)
            {
                foreach (var includeExpression in includeExpressions)
                {
                    query = query.Include(includeExpression);
                }
            }
            if (includeStrings != null)
            {
                foreach (var includeString in includeStrings)
                {
                    query = query.Include(includeString);
                }
            }
            return query;
        }
    }
}
