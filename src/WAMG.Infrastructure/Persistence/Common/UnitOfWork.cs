﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using WAMG.Core.Model;
using WAMG.Core.Repository;
using WAMG.Core.Service;
using WAMG.Infrastructure.Persistence;

namespace WAMG.Infrastructure.Persistence.Common
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly IDateTimeService _dateTime;

        public UnitOfWork(ApplicationDbContext context, IDateTimeService dateTimeService)
        {
            _context = context;
            _dateTime = dateTimeService;
        }

        public Task Commit()
        {
            foreach (var entry in _context.ChangeTracker.Entries<BaseEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedAt = _dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastUpdatedAt = _dateTime.Now;
                        break;
                }
            }

            return _context.SaveChangesAsync();
        }
    }
}
