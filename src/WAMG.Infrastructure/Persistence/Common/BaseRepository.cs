﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WAMG.Core.Model;
using WAMG.Core.Repository;
using WAMG.Core.Repository.Pagination;
using WAMG.Core.Repository.Specification;

namespace WAMG.Infrastructure.Persistence.Common
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly IQueryRepository<TEntity> _queryRepository;
        protected readonly IMutationRepository<TEntity> _mutationRepository;

        public BaseRepository(IQueryRepository<TEntity> queryRepository, IMutationRepository<TEntity> mutationRepository)
        {
            _queryRepository = queryRepository;
            _mutationRepository = mutationRepository;
        }

        // as we can't inherit from two parents (multiple inheritance), we'll resort to composition

        #region BaseQueryRepository
        public IQueryable<TEntity> GetQueryable() => _queryRepository.GetQueryable();
        public Task<IPaginationResult<TEntity>> GetPaginatedList(IPaginationQuery paginationQuery, IQuerySpecification<TEntity> spec = null) => _queryRepository.GetPaginatedList(paginationQuery, spec);
        public Task<List<TEntity>> GetList(IQuerySpecification<TEntity> spec = null) => _queryRepository.GetList(spec);
        public Task<List<TEntity>> GetList(Expression<Func<TEntity, bool>> predicate = null) => _queryRepository.GetList(predicate);
        public Task<TEntity> GetSingle(IQuerySpecification<TEntity> spec = null) => _queryRepository.GetSingle(spec);
        public Task<TEntity> GetSingle(Expression<Func<TEntity, bool>> predicate = null) => _queryRepository.GetSingle(predicate);
        public Task<TEntity> GetById(int id) => _queryRepository.GetById(id);
        public Task<bool> Contains(IQuerySpecification<TEntity> spec = null) => _queryRepository.Contains(spec);
        public Task<bool> Contains(Expression<Func<TEntity, bool>> predicate = null) => _queryRepository.Contains(predicate);
        public Task<bool> Contains(int id) => _queryRepository.Contains(id);
        #endregion

        #region BaseMutationRepository
        public Task Add(TEntity entity) => _mutationRepository.Add(entity);
        public Task AddRange(IEnumerable<TEntity> entities) => _mutationRepository.AddRange(entities);
        public Task Update(TEntity entity) => _mutationRepository.Update(entity);
        public Task UpdateRange(IEnumerable<TEntity> entities) => _mutationRepository.UpdateRange(entities);
        public Task Remove(TEntity entity) => _mutationRepository.Remove(entity);
        public Task Remove(int id) => _mutationRepository.Remove(id);
        public Task RemoveRange(IEnumerable<TEntity> entities) => _mutationRepository.RemoveRange(entities);
        #endregion
    }
}
