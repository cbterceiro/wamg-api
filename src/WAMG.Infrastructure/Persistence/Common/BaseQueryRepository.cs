﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WAMG.Core.Model;
using WAMG.Core.Repository;
using WAMG.Core.Repository.Pagination;
using WAMG.Core.Repository.Specification;
using WAMG.Infrastructure.Persistence.Common.Pagination;
using WAMG.Infrastructure.Persistence.Common.Specification;

namespace WAMG.Infrastructure.Persistence.Common
{
    public class BaseQueryRepository<T> : IQueryRepository<T> where T : BaseEntity
    {
        protected readonly ApplicationDbContext _context;
        protected readonly DbSet<T> _dbSet;

        public BaseQueryRepository(ApplicationDbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public IQueryable<T> GetQueryable()
        {
            return _dbSet.AsQueryable();
        }

        public Task<IPaginationResult<T>> GetPaginatedList(IPaginationQuery paginationQuery, IQuerySpecification<T> spec = null)
        {
            var query = QuerySpecificationEvaluator<T>.ApplySpecification(_dbSet.AsQueryable(), spec);

            var paginationResult = PaginationEvaluator<T>.ApplyPaging(query, paginationQuery);

            return paginationResult;
        }

        public Task<List<T>> GetList(IQuerySpecification<T> spec = null)
        {
            var query = QuerySpecificationEvaluator<T>.ApplySpecification(_dbSet.AsQueryable(), spec);

            return query.ToListAsync();
        }

        public Task<List<T>> GetList(Expression<Func<T, bool>> predicate = null)
        {
            var query = _dbSet.AsQueryable();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.ToListAsync();
        }

        public Task<T> GetSingle(IQuerySpecification<T> spec = null)
        {
            var query = QuerySpecificationEvaluator<T>.ApplySpecification(_dbSet.AsQueryable(), spec);

            return query.SingleOrDefaultAsync();
        }

        public Task<T> GetSingle(Expression<Func<T, bool>> predicate = null)
        {
            var query = _dbSet.AsQueryable();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.SingleOrDefaultAsync();
        }

        public Task<T> GetById(int id)
        {
            return _dbSet.FindAsync(id).AsTask();
        }

        public Task<bool> Contains(IQuerySpecification<T> spec = null)
        {
            var query = QuerySpecificationEvaluator<T>.ApplySpecification(_dbSet.AsQueryable(), spec);
            return query.AnyAsync();
        }

        public Task<bool> Contains(Expression<Func<T, bool>> predicate = null)
        {
            var query = _dbSet.AsQueryable();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.AnyAsync();
        }

        public Task<bool> Contains(int id)
        {
            return _dbSet.AnyAsync(e => e.Id == id);
        }
    }
}
