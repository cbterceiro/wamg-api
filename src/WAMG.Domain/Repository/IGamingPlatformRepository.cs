﻿using WAMG.Core.Repository;
using WAMG.Domain.Entities;

namespace WAMG.Domain.Repository
{
    public interface IGamingPlatformRepository : IRepository<GamingPlatform>
    {
    }
}
