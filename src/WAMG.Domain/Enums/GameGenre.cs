﻿namespace WAMG.Domain.Enums
{
    public enum GameGenre
    {
        Adventure,
        Action,
        Educational,
        Fighting,
        Horror,
        Metroidvania,
        MOBA,
        Racing,
        RTS,
        RPG,
        Sandbox,
        Shooter,
        Sports,
        Survival,
    }
}
