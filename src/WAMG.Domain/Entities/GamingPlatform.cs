﻿using WAMG.Core.Model;

namespace WAMG.Domain.Entities
{
    public class GamingPlatform : BaseEntity
    {
        public string Name { get; set; }
        public bool IsConsole { get; set; }
        public bool IsHandheld { get; set; }
    }
}
