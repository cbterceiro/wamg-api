using WAMG.Core.Model;
using WAMG.Domain.Enums;

namespace WAMG.Domain.Entities
{
    public class Game : BaseEntity
    {
        public string Name { get; set; }
        public GameGenre Genre { get; set; }
        public int PlatformId { get; set; }
        public GamingPlatform Platform { get; set; }
        public int? LoanedToId { get; set; }
        public Friend LoanedTo { get; set; }
    }
}
