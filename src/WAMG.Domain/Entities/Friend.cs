using WAMG.Core.Model;

namespace WAMG.Domain.Entities
{
    public class Friend : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
