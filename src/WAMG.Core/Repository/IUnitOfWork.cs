﻿using System.Threading.Tasks;

namespace WAMG.Core.Repository
{
    public interface IUnitOfWork
    {
        public Task Commit();
    }
}
