﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WAMG.Core.Model;

namespace WAMG.Core.Repository.Specification
{
    public interface IQuerySpecification<TEntity> where TEntity : BaseEntity
    {
        public IList<Expression<Func<TEntity, bool>>> FilterExpressions { get; }
        public IList<IOrderByExpression<TEntity>> OrderByExpressions { get; }
        public IList<Expression<Func<TEntity, object>>> IncludeExpressions { get; }
        public IList<string> IncludeStrings { get; }
    }
}
