﻿using System;
using System.Linq.Expressions;
using WAMG.Core.Model;

namespace WAMG.Core.Repository.Specification
{
    public enum OrderByExpressionDirection
    {
        Ascending = 1,
        Descending = 2,
    }

    public interface IOrderByExpression<T> where T : BaseEntity
    {
        public Expression<Func<T, object>> Expression { get; set; }
        public OrderByExpressionDirection Direction { get; set; }
    }
}
