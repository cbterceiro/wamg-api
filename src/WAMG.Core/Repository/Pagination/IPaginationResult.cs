﻿using System.Collections.Generic;

namespace WAMG.Core.Repository.Pagination
{
    public interface IPaginationResult<T>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public IEnumerable<T> Items { get; set; }
        public int TotalPages { get; set; }
        public int TotalCount { get; set; }
        public bool HasPreviousPage { get; set; } 
        public bool HasNextPage { get; set; }
    }
}
