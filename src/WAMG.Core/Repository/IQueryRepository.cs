﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WAMG.Core.Model;
using WAMG.Core.Repository.Pagination;
using WAMG.Core.Repository.Specification;

namespace WAMG.Core.Repository
{
    public interface IQueryRepository<TEntity> where TEntity : BaseEntity
    {
        public IQueryable<TEntity> GetQueryable();
        public Task<IPaginationResult<TEntity>> GetPaginatedList(IPaginationQuery paginationQuery, IQuerySpecification<TEntity> spec = null);
        public Task<List<TEntity>> GetList(IQuerySpecification<TEntity> spec = null);
        public Task<List<TEntity>> GetList(Expression<Func<TEntity, bool>> predicate = null);
        public Task<TEntity> GetSingle(IQuerySpecification<TEntity> spec = null);
        public Task<TEntity> GetSingle(Expression<Func<TEntity, bool>> predicate = null);
        public Task<TEntity> GetById(int id);
        public Task<bool> Contains(IQuerySpecification<TEntity> spec = null);
        public Task<bool> Contains(Expression<Func<TEntity, bool>> predicate = null);
        public Task<bool> Contains(int id);
    }
}
