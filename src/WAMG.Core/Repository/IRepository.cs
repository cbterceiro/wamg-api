﻿using WAMG.Core.Model;

namespace WAMG.Core.Repository
{
    public interface IRepository<T> : IQueryRepository<T>, IMutationRepository<T> where T : BaseEntity
    {
    }
}
