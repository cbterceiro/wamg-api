﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WAMG.Core.Model;

namespace WAMG.Core.Repository
{
    public interface IMutationRepository<T> where T : BaseEntity
    {
        public Task Add(T entity);
        public Task AddRange(IEnumerable<T> entities);
        public Task Update(T entity);
        public Task UpdateRange(IEnumerable<T> entities);
        public Task Remove(T entity);
        public Task Remove(int id);
        public Task RemoveRange(IEnumerable<T> entities);
    }
}
