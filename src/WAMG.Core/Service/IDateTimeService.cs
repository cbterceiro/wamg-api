﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAMG.Core.Service
{
    public interface IDateTimeService
    {
        public DateTime Now { get; }
    }
}
