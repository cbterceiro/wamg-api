﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using WAMG.Application.Commands.Games.CreateGame;
using WAMG.Application.Commands.Games.LoanGameToFriend;
using WAMG.Application.Commands.Games.RemoveGame;
using WAMG.Application.Commands.Games.ReturnGameFromFriend;
using WAMG.Application.Commands.Games.UpdateGame;
using WAMG.Application.Queries.Games;
using WAMG.Application.Queries.Games.GetGameById;
using WAMG.Application.Queries.Games.GetGamesWithPagination;
using WAMG.Application.Queries.Games.GetGenres;

namespace WAMG.WebAPI.Controllers
{
    public class GameController : ApiControllerBase
    {
        [HttpGet("Genres")]
        public async Task<ActionResult<List<string>>> GetGenres()
        {
            var genres = await _mediator.Send(new GetGenresQuery());
            return Ok(genres);
        }

        [HttpGet]
        public async Task<ActionResult<GameDTO>> GetGames([FromQuery] GetGamesWithPaginationQuery request)
        {
            var games = await _mediator.Send(request);
            return Ok(games);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<GameDetailDTO>> GetGameById([FromRoute] int id)
        {
            var game = await _mediator.Send(new GetGameByIdQuery(id));
            return Ok(game);
        }

        [HttpPost]
        public async Task<ActionResult> CreateGame([FromBody] CreateGameCommand request)
        {
            var gameId = await _mediator.Send(request);
            return CreatedAtAction(nameof(CreateGame), new { Id = gameId });
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> UpdateGame([FromRoute] int id, [FromBody] UpdateGameCommand request)
        {
            if (id != request.Id)
            {
                return BadRequest();
            }
            await _mediator.Send(request);
            return Accepted();
        }

        [HttpPatch("{gameId:int}/LoanTo/{friendId:int}")]
        public async Task<ActionResult> LoanGameToFriend([FromRoute] int gameId, [FromRoute] int friendId)
        {
            await _mediator.Send(new LoanGameToFriendCommand { GameId = gameId, FriendId = friendId });
            return Accepted();
        }

        [HttpPatch("{id:int}/Return")]
        public async Task<ActionResult> ReturnGameFromFriend([FromRoute] int id)
        {
            await _mediator.Send(new ReturnGameFromFriendCommand { Id = id });
            return Accepted();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> RemoveGame([FromRoute] int id)
        {
            await _mediator.Send(new RemoveGameCommand { Id = id });
            return Accepted();
        }
    }
}
