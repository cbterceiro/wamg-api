﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace WAMG.WebAPI.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    public class ApiControllerBase : ControllerBase
    {
        private IMediator Mediator;
        protected IMediator _mediator => Mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    }
}
