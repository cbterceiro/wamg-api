﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WAMG.Application.Commands.Friends.CreateFriend;
using WAMG.Application.Commands.Friends.RemoveFriend;
using WAMG.Application.Commands.Friends.UpdateFriend;
using WAMG.Application.Queries.Friends;
using WAMG.Application.Queries.Friends.GetFriendById;
using WAMG.Application.Queries.Friends.GetFriendsWithPagination;
using WAMG.Core.Repository.Pagination;

namespace WAMG.WebAPI.Controllers
{
    public class FriendController : ApiControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<IPaginationResult<FriendDTO>>> GetFriends([FromQuery] GetFriendsWithPaginationQuery request)
        {
            var friends = await _mediator.Send(request);
            return Ok(friends);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<FriendDTO>> GetFriendById([FromRoute] int id)
        {
            var friend = await _mediator.Send(new GetFriendByIdQuery(id));
            return Ok(friend);
        }

        [HttpPost]
        public async Task<ActionResult> CreateFriend([FromBody] CreateFriendCommand request)
        {
            var friendId = await _mediator.Send(request);
            return CreatedAtAction(nameof(CreateFriend), new { Id = friendId });
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> UpdateFriend([FromRoute] int id, [FromBody] UpdateFriendCommand request)
        {
            if (id != request.Id)
            {
                return BadRequest();
            }
            await _mediator.Send(request);
            return Accepted();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> RemoveFriend([FromRoute] int id)
        {
            await _mediator.Send(new RemoveFriendCommand { Id = id });
            return Accepted();
        }
    }
}
