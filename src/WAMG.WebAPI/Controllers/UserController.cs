﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WAMG.Application.Commands.ApplicationUsers.Authenticate;

namespace WAMG.WebAPI.Controllers
{
    public class UserController : ApiControllerBase
    {
        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public async Task<ActionResult<AuthenticationResponse>> Authenticate([FromBody] AuthenticateCommand request)
        {
            var response = await _mediator.Send(request);
            return Ok(response);
        }
    }
}
