﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Application.Commands.GamingPlatforms.CreateGamingPlatform;
using WAMG.Application.Commands.GamingPlatforms.RemoveGamingPlatform;
using WAMG.Application.Commands.GamingPlatforms.UpdateGamingPlatform;
using WAMG.Application.Queries.GamingPlatforms;
using WAMG.Application.Queries.GamingPlatforms.GetGamingPlatformById;
using WAMG.Application.Queries.GamingPlatforms.GetGamingPlatforms;

namespace WAMG.WebAPI.Controllers
{
    public class GamingPlatformController : ApiControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<GamingPlatformDTO>> GetGamingPlatforms([FromQuery] GetGamingPlatformsQuery request)
        {
            var gamingPlatforms = await _mediator.Send(request);
            return Ok(gamingPlatforms);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<GamingPlatformDTO>> GetGamingPlatformById([FromRoute] int id)
        {
            var gamingPlatform = await _mediator.Send(new GetGamingPlatformByIdQuery(id));
            return Ok(gamingPlatform);
        }

        [HttpPost]
        public async Task<ActionResult> CreateGamingPlatform([FromBody] CreateGamingPlatformCommand request)
        {
            var gamingPlatformId = await _mediator.Send(request);
            return CreatedAtAction(nameof(CreateGamingPlatform), new { Id = gamingPlatformId });
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> UpdateGamingPlatform([FromRoute] int id, [FromBody] UpdateGamingPlatformCommand request)
        {
            if (id != request.Id)
            {
                return BadRequest();
            }
            await _mediator.Send(request);
            return Accepted();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> RemoveGamingPlatform([FromRoute] int id)
        {
            await _mediator.Send(new RemoveGamingPlatformCommand { Id = id });
            return Accepted();
        }
    }
}
