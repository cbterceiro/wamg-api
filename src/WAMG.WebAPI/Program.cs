using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WAMG.Infrastructure.Identity;
using WAMG.Infrastructure.Persistence;

namespace WAMG.WebAPI
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                try
                {
                    await EnsureDatabase(scope);
                    await SeedDefaultUser(scope);
                    await SeedGamingPlatforms(scope);
                }
                catch (Exception ex)
                {
                    var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();

                    logger.LogError(ex, "An error occurred while migrating or seeding the database.");

                    throw;
                }
            }
            
            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddEnvironmentVariables(prefix: "WAMG_");
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });


        private static async Task EnsureDatabase(IServiceScope serviceScope)
        {
            using var identityContext = serviceScope.ServiceProvider.GetService<ApplicationIdentityDbContext>();
            await identityContext.Database.MigrateAsync();

            using var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
            await context.Database.MigrateAsync();
        }

        private static async Task SeedDefaultUser(IServiceScope serviceScope)
        {
            var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

            await ApplicationIdentityDbContextSeed.SeedDefaultUsersAsync(userManager, roleManager);
        }

        private static async Task SeedGamingPlatforms(IServiceScope serviceScope)
        {
            using var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
            await ApplicationDbContextSeed.SeedGamingPlatforms(context);
        }
    }
}
