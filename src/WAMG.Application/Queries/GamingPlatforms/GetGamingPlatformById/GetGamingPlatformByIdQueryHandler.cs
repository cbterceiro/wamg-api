﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Queries.GamingPlatforms.GetGamingPlatformById
{
    public class GetGamingPlatformByIdQueryHandler : IRequestHandler<GetGamingPlatformByIdQuery, GamingPlatformDTO>
    {
        private readonly IGamingPlatformRepository _gamingPlatformRepository;
        private readonly IMapper _mapper;

        public GetGamingPlatformByIdQueryHandler(IGamingPlatformRepository gamingPlatformRepository, IMapper mapper)
        {
            _gamingPlatformRepository = gamingPlatformRepository;
            _mapper = mapper;
        }

        public async Task<GamingPlatformDTO> Handle(GetGamingPlatformByIdQuery request, CancellationToken cancellationToken)
        {
            var gamingPlatformId = request.Id;
            var gamingPlatform = await _gamingPlatformRepository.GetById(gamingPlatformId);

            if (gamingPlatform == null)
            {
                throw new EntityNotFoundException(nameof(GamingPlatform), gamingPlatformId);
            }

            return _mapper.Map<GamingPlatformDTO>(gamingPlatform);
        }
    }
}
