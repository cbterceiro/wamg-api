﻿using MediatR;

namespace WAMG.Application.Queries.GamingPlatforms.GetGamingPlatformById
{
    public class GetGamingPlatformByIdQuery : IRequest<GamingPlatformDTO>
    {
        public int Id { get; private set; }

        public GetGamingPlatformByIdQuery() { }
        public GetGamingPlatformByIdQuery(int id)
        {
            Id = id;
        }
    }
}
