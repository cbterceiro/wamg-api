﻿using MediatR;
using System.Collections.Generic;

namespace WAMG.Application.Queries.GamingPlatforms.GetGamingPlatforms
{
    public class GetGamingPlatformsQuery : IRequest<List<GamingPlatformDTO>>
    {
        public string Name { get; set; }
        public bool OnlyConsole { get; set; }
        public bool OnlyHandheld { get; set; }
    }
}
