﻿using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Queries.GamingPlatforms.Specification;
using WAMG.Domain.Repository;

namespace WAMG.Application.Queries.GamingPlatforms.GetGamingPlatforms
{
    public class GetGamingPlatformsQueryHandler : IRequestHandler<GetGamingPlatformsQuery, List<GamingPlatformDTO>>
    {
        private readonly IGamingPlatformRepository _gamingPlatformRepository;
        private readonly IMapper _mapper;

        public GetGamingPlatformsQueryHandler(IGamingPlatformRepository gamingPlatformRepository, IMapper mapper)
        {
            _gamingPlatformRepository = gamingPlatformRepository;
            _mapper = mapper;
        }

        public async Task<List<GamingPlatformDTO>> Handle(GetGamingPlatformsQuery request, CancellationToken cancellationToken)
        {
            var spec = new GamingPlatformsFilteredOrderedSpecification(request.Name, request.OnlyConsole, request.OnlyHandheld);
            var gamingPlatforms = await _gamingPlatformRepository.GetList(spec);
            return _mapper.Map<List<GamingPlatformDTO>>(gamingPlatforms);
        }
    }
}
