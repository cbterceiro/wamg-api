﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAMG.Application.Queries.GamingPlatforms.GetGamingPlatforms
{
    public class GetGamingPlatformsQueryValidator : AbstractValidator<GetGamingPlatformsQuery>
    {
        public GetGamingPlatformsQueryValidator()
        {
            RuleFor(q => q.Name)
                .MaximumLength(100)
                .WithMessage("The name query filter must not exceed 100 characters");
        }
    }
}
