﻿using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Queries.GamingPlatforms
{
    public class GamingPlatformDTO : IMapFrom<GamingPlatform>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsConsole { get; set; }
        public bool IsHandheld { get; set; }
    }
}
