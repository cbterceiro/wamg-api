﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAMG.Application.Common.Repository.Specification;
using WAMG.Domain.Entities;

namespace WAMG.Application.Queries.GamingPlatforms.Specification
{
    public class GamingPlatformsFilteredOrderedSpecification : BaseSpecification<GamingPlatform>
    {
        public GamingPlatformsFilteredOrderedSpecification(string name, bool onlyConsole, bool onlyHandheld)
        {
            if (!string.IsNullOrEmpty(name))
            {
                AddFilter(g => g.Name.Contains(name));
            }
            if (onlyConsole)
            {
                AddFilter(g => g.IsConsole);
            }
            if (onlyHandheld)
            {
                AddFilter(g => g.IsHandheld);
            }

            AddOrderBy(g => g.Name);
        }
    }
}
