﻿using MediatR;
using System.Collections.Generic;

namespace WAMG.Application.Queries.Games.GetGenres
{
    public class GetGenresQuery : IRequest<List<string>>
    {
    }
}
