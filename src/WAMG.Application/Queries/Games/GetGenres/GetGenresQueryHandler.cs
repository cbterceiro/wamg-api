﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Domain.Enums;

namespace WAMG.Application.Queries.Games.GetGenres
{
    public class GetGenresQueryHandler : IRequestHandler<GetGenresQuery, List<string>>
    {
        public Task<List<string>> Handle(GetGenresQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(Enum.GetValues<GameGenre>().Select(g => g.ToString()).ToList());
        }
    }
}
