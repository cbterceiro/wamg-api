﻿using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Queries.Games
{
    public class GameDTO : IMapFrom<Game>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public GamingPlatformSimpleDTO Platform { get; set; }
        public FriendSimpleDTO LoanedTo { get; set; }
    }

    public class GamingPlatformSimpleDTO : IMapFrom<GamingPlatform>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class FriendSimpleDTO : IMapFrom<Friend>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
