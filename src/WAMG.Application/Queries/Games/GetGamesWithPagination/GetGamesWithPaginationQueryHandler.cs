﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Queries.Games.Specification;
using WAMG.Core.Repository.Pagination;
using WAMG.Domain.Repository;

namespace WAMG.Application.Queries.Games.GetGamesWithPagination
{
    public class GetGamesWithPaginationQueryHandler : IRequestHandler<GetGamesWithPaginationQuery, IPaginationResult<GameDTO>>
    {
        private readonly IGameRepository _gameRepository;
        private readonly IMapper _mapper;

        public GetGamesWithPaginationQueryHandler(IGameRepository gameRepository, IMapper mapper)
        {
            _gameRepository = gameRepository;
            _mapper = mapper;
        }

        public async Task<IPaginationResult<GameDTO>> Handle(GetGamesWithPaginationQuery request, CancellationToken cancellationToken)
        {
            var spec = new GamesFilteredOrderedByNameIncludingPlatformAndLoanSpecification(request.Name, request.Genre, request.GamingPlatformId, request.FriendId);
            var gamesPage = await _gameRepository.GetPaginatedList(request, spec);
            return _mapper.Map<IPaginationResult<GameDTO>>(gamesPage);
        }
    }
}
