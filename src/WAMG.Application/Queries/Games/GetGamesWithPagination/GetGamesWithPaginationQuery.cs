﻿using MediatR;
using WAMG.Core.Repository.Pagination;
using WAMG.Domain.Enums;

namespace WAMG.Application.Queries.Games.GetGamesWithPagination
{
    public class GetGamesWithPaginationQuery : IRequest<IPaginationResult<GameDTO>>, IPaginationQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Name { get; set; }
        public GameGenre? Genre { get; set; }
        public int? GamingPlatformId { get; set; }
        public int? FriendId { get; set; }

        public GetGamesWithPaginationQuery()
        {
            PageNumber = 1;
            PageSize = 20;
        }
    }
}
