﻿using FluentValidation;
using WAMG.Application.Common.Repository.Validation;
using WAMG.Domain.Enums;

namespace WAMG.Application.Queries.Games.GetGamesWithPagination
{
    public class GetGamesWithPaginationQueryValidator : AbstractValidator<GetGamesWithPaginationQuery>
    {
        public GetGamesWithPaginationQueryValidator()
        {
            Include(new PaginationQueryValidator());

            RuleFor(q => q.Name)
                .MaximumLength(100)
                    .WithMessage("The name query filter must not exceed 100 characters");

            RuleFor(q => q.GamingPlatformId)
                .GreaterThanOrEqualTo(1)
                    .WithMessage("The gaming platform identifier must be greater than or equal to 1");

            RuleFor(q => q.FriendId)
                .GreaterThanOrEqualTo(1)
                    .WithMessage("The friend identifier must be greater than or equal to 1");
        }
    }
}
