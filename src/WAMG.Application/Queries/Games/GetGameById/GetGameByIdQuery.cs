﻿using MediatR;

namespace WAMG.Application.Queries.Games.GetGameById
{
    public class GetGameByIdQuery : IRequest<GameDetailDTO>
    {
        public int Id { get; private set; }

        public GetGameByIdQuery() { }
        public GetGameByIdQuery(int id)
        {
            Id = id;
        }
    }
}
