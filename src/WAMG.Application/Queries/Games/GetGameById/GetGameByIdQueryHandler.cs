﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Application.Queries.Games.Specification;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Queries.Games.GetGameById
{
    public class GetGameByIdQueryHandler : IRequestHandler<GetGameByIdQuery, GameDetailDTO>
    {
        private readonly IGameRepository _gameRepository;
        private readonly IMapper _mapper;

        public GetGameByIdQueryHandler(IGameRepository gameRepository, IMapper mapper)
        {
            _gameRepository = gameRepository;
            _mapper = mapper;
        }

        public async Task<GameDetailDTO> Handle(GetGameByIdQuery request, CancellationToken cancellationToken)
        {
            var gameId = request.Id;
            var game = await _gameRepository.GetSingle(new GameByIdWithPlatformAndFriend(gameId));

            if (game == null)
            {
                throw new EntityNotFoundException(nameof(Game), gameId);
            }

            return _mapper.Map<GameDetailDTO>(game);
        }
    }
}
