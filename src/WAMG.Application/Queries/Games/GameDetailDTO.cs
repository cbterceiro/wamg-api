﻿using WAMG.Application.Common.Mapping;
using WAMG.Application.Queries.Friends;
using WAMG.Application.Queries.GamingPlatforms;
using WAMG.Domain.Entities;

namespace WAMG.Application.Queries.Games
{
    public class GameDetailDTO : IMapFrom<Game>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public GamingPlatformDTO Platform { get; set; }
        public FriendDTO LoanedTo { get; set; }
    }
}
