﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAMG.Application.Common.Repository.Specification;
using WAMG.Domain.Entities;
using WAMG.Domain.Enums;

namespace WAMG.Application.Queries.Games.Specification
{
    class GamesFilteredOrderedByNameIncludingPlatformAndLoanSpecification : BaseSpecification<Game>
    {
        public GamesFilteredOrderedByNameIncludingPlatformAndLoanSpecification(string name, GameGenre? genre, int? gamingPlatformId, int? friendId)
        {
            if (!string.IsNullOrEmpty(name))
            {
                AddFilter(g => g.Name.Contains(name));
            }
            if (genre.HasValue)
            {
                AddFilter(g => g.Genre.Equals(genre));
            }
            if (gamingPlatformId.HasValue)
            {
                AddFilter(g => g.PlatformId.Equals(gamingPlatformId));
            }
            if (friendId.HasValue)
            {
                AddFilter(g => g.LoanedToId.Equals(friendId));
            }

            AddInclude(g => g.Platform);
            AddInclude(g => g.LoanedTo);

            AddOrderBy(g => g.Name);
        }
    }
}
