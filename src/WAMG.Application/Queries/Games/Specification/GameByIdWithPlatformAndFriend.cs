﻿using WAMG.Application.Common.Repository.Specification;
using WAMG.Domain.Entities;

namespace WAMG.Application.Queries.Games.Specification
{
    public class GameByIdWithPlatformAndFriend : BaseSpecification<Game>
    {
        public GameByIdWithPlatformAndFriend(int id)
        {
            AddFilter(g => g.Id == id);
            AddInclude(g => g.Platform);
            AddInclude(g => g.LoanedTo);
        }
    }
}
