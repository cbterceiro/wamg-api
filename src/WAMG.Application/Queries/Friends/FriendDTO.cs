﻿using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Queries.Friends
{
    public class FriendDTO : IMapFrom<Friend>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
