﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAMG.Application.Common.Repository.Specification;
using WAMG.Domain.Entities;

namespace WAMG.Application.Queries.Friends.Specification
{
    public class FriendsByNameOrderedSpecification : BaseSpecification<Friend>
    {
        public FriendsByNameOrderedSpecification(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                AddFilter(f => f.Name.Contains(name.ToLower()));
            }

            AddOrderBy(f => f.Name);
        }
    }
}
