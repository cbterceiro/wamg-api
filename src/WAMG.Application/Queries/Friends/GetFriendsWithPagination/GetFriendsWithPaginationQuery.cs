﻿using MediatR;
using WAMG.Core.Repository.Pagination;

namespace WAMG.Application.Queries.Friends.GetFriendsWithPagination
{
    public class GetFriendsWithPaginationQuery : IRequest<IPaginationResult<FriendDTO>>, IPaginationQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Name { get; set; }

        public GetFriendsWithPaginationQuery()
        {
            PageNumber = 1;
            PageSize = 20;
        }
    }
}
