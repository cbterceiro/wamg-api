﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Queries.Friends.Specification;
using WAMG.Core.Repository.Pagination;
using WAMG.Domain.Repository;

namespace WAMG.Application.Queries.Friends.GetFriendsWithPagination
{
    public class GetFriendsWithPaginationQueryHandler : IRequestHandler<GetFriendsWithPaginationQuery, IPaginationResult<FriendDTO>>
    {
        private readonly IFriendRepository _friendRepository;
        private readonly IMapper _mapper;

        public GetFriendsWithPaginationQueryHandler(IFriendRepository friendRepository, IMapper mapper)
        {
            _friendRepository = friendRepository;
            _mapper = mapper;
        }

        public async Task<IPaginationResult<FriendDTO>> Handle(GetFriendsWithPaginationQuery request, CancellationToken cancellationToken)
        {
            var friendsPage = await _friendRepository.GetPaginatedList(request, new FriendsByNameOrderedSpecification(request.Name));
            return _mapper.Map<IPaginationResult<FriendDTO>>(friendsPage);
        }
    }
}
