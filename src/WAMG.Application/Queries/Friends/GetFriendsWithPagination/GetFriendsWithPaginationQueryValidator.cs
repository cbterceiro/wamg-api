﻿using FluentValidation;
using WAMG.Application.Common.Repository.Validation;

namespace WAMG.Application.Queries.Friends.GetFriendsWithPagination
{
    public class GetFriendsWithPaginationQueryValidator : AbstractValidator<GetFriendsWithPaginationQuery>
    {
        public GetFriendsWithPaginationQueryValidator()
        {
            Include(new PaginationQueryValidator());

            RuleFor(q => q.Name)
                .MaximumLength(100)
                .WithMessage("The name query filter must not exceed 100 characters");
        }
    }
}
