﻿using MediatR;

namespace WAMG.Application.Queries.Friends.GetFriendById
{
    public class GetFriendByIdQuery : IRequest<FriendDTO>
    {
        public int Id { get; private set; }

        public GetFriendByIdQuery() { }
        public GetFriendByIdQuery(int id)
        {
            Id = id;
        }
    }
}
