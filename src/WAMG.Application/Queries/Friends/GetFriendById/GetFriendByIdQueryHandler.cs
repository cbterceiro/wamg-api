﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Queries.Friends.GetFriendById
{
    public class GetFriendByIdQueryHandler : IRequestHandler<GetFriendByIdQuery, FriendDTO>
    {
        private readonly IFriendRepository _friendRepository;
        private readonly IMapper _mapper;

        public GetFriendByIdQueryHandler(IFriendRepository friendRepository, IMapper mapper)
        {
            _friendRepository = friendRepository;
            _mapper = mapper;
        }

        public async Task<FriendDTO> Handle(GetFriendByIdQuery request, CancellationToken cancellationToken)
        {
            var friendId = request.Id;
            var friend = await _friendRepository.GetById(friendId);

            if (friend == null)
            {
                throw new EntityNotFoundException(nameof(Friend), friendId);
            }

            return _mapper.Map<FriendDTO>(friend);
        }
    }
}
