﻿using System;
using WAMG.Core.Service;

namespace WAMG.Application.Common.Service
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime Now => DateTime.Now;
    }
}
