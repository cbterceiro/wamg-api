﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WAMG.Core.Model;
using WAMG.Core.Repository.Specification;

namespace WAMG.Application.Common.Repository.Specification
{
    public class OrderByExpression<T> : IOrderByExpression<T> where T : BaseEntity
    {
        public Expression<Func<T, object>> Expression { get; set; }
        public OrderByExpressionDirection Direction { get; set; }

        public OrderByExpression(Expression<Func<T, object>> expression)
        {
            Expression = expression;
            Direction = OrderByExpressionDirection.Ascending;
        }

        public OrderByExpression(Expression<Func<T, object>> expression, OrderByExpressionDirection direction)
        {
            Expression = expression;
            Direction = direction;
        }
    }
}
