﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WAMG.Core.Model;
using WAMG.Core.Repository.Specification;

namespace WAMG.Application.Common.Repository.Specification
{
    public class BaseSpecification<T> : IQuerySpecification<T> where T : BaseEntity
    {
        public IList<Expression<Func<T, bool>>> FilterExpressions { get; }
        public IList<IOrderByExpression<T>> OrderByExpressions { get; }
        public IList<Expression<Func<T, object>>> IncludeExpressions { get; }
        public IList<string> IncludeStrings { get; }


        public BaseSpecification()
        {
            FilterExpressions = new List<Expression<Func<T, bool>>>();
            OrderByExpressions = new List<IOrderByExpression<T>>();
            IncludeExpressions = new List<Expression<Func<T, object>>>();
            IncludeStrings = new List<string>();
        }

        protected void AddFilter(Expression<Func<T, bool>> expression)
        {
            FilterExpressions.Add(expression);
        }

        protected void AddOrderBy(Expression<Func<T, object>> expression)
        {
            OrderByExpressions.Add(new OrderByExpression<T>(expression));
        }

        protected void AddOrderBy(Expression<Func<T, object>> expression, OrderByExpressionDirection direction)
        {
            OrderByExpressions.Add(new OrderByExpression<T>(expression, direction));
        }

        protected void AddOrderBy(IOrderByExpression<T> expression)
        {
            OrderByExpressions.Add(expression);
        }

        protected void AddInclude(Expression<Func<T, object>> expression)
        {
            IncludeExpressions.Add(expression);
        }

        protected void AddInclude(string includeString)
        {
            IncludeStrings.Add(includeString);
        }
    }
}

