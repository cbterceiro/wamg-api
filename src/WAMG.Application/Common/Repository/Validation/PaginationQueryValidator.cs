﻿using FluentValidation;
using WAMG.Core.Repository.Pagination;

namespace WAMG.Application.Common.Repository.Validation
{
    public class PaginationQueryValidator : AbstractValidator<IPaginationQuery>
    {
        public PaginationQueryValidator()
        {
            RuleFor(q => q.PageNumber)
                .GreaterThanOrEqualTo(1)
                .WithMessage("The page number must be greater than or equal to 1");

            RuleFor(q => q.PageSize)
                .InclusiveBetween(1, 100)
                .WithMessage("The page size must be between 1 and 100 items");
        }
    }
}
