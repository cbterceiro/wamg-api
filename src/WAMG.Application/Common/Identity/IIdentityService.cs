﻿using System.Threading.Tasks;

namespace WAMG.Application.Common.Identity
{
    public interface IIdentityService
    {
        public Task<string> AuthenticateAndGenerateToken(string username, string password);
    }
}
