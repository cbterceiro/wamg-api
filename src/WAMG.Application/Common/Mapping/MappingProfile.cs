﻿using AutoMapper;
using System;
using System.Linq;
using System.Reflection;
using WAMG.Core.Repository.Pagination;

namespace WAMG.Application.Common.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());

            // enable automapping of pagination result from one type to another
            CreateMap(typeof(IPaginationResult<>), typeof(IPaginationResult<>));
        }

        private void ApplyMappingsFromAssembly(Assembly assembly)
        {
            var types = assembly.GetExportedTypes()
                .Where(t => t.GetInterfaces().Any(i => i.IsGenericType && (
                    i.GetGenericTypeDefinition() == typeof(IMapFrom<>) || i.GetGenericTypeDefinition() == typeof(IMapTo<>)
                 )))
                .Where(t => !t.ContainsGenericParameters)
                .ToList();

            foreach (var type in types)
            {
                var instance = Activator.CreateInstance(type);

                var methodInfo = type.GetMethod("Mapping");

                if (methodInfo == null)
                {
                    var mappingInterface = type.GetInterface("IMapFrom`1") ?? type.GetInterface("IMapTo`1");

                    methodInfo = mappingInterface.GetMethod("Mapping");
                }

                methodInfo?.Invoke(instance, new object[] { this });
            }
        }
    }
}
