﻿using MediatR;

namespace WAMG.Application.Commands.Games.LoanGameToFriend
{
    public class LoanGameToFriendCommand : IRequest<bool>
    {
        public int GameId { get; set; }
        public int FriendId { get; set; }
    }
}
