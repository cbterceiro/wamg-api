﻿using FluentValidation;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Games.LoanGameToFriend
{
    public class LoanGameToFriendCommandValidator : AbstractValidator<LoanGameToFriendCommand>
    {
        private readonly IFriendRepository _friendRepository;

        public LoanGameToFriendCommandValidator(IFriendRepository friendRepository)
        {
            _friendRepository = friendRepository;

            RuleFor(g => g.FriendId)
                .MustAsync(ExistOnDatabase)
                    .WithMessage("The friend {PropertyValue} was not found on the database");
        }

        private Task<bool> ExistOnDatabase(int friendI, CancellationToken cancellationToken)
        {
            return _friendRepository.Contains(f => f.Id == friendI);
        }
    }
}
