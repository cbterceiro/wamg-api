﻿using MediatR;

namespace WAMG.Application.Commands.Games.RemoveGame
{
    public class RemoveGameCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public RemoveGameCommand() { }
        public RemoveGameCommand(int id)
        {
            Id = id;
        }
    }
}
