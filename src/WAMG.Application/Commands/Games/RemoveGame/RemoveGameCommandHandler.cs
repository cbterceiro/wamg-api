﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Games.RemoveGame
{
    public class RemoveGameCommandHandler : IRequestHandler<RemoveGameCommand, bool>
    {
        private readonly IGameRepository _gameRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RemoveGameCommandHandler(IGameRepository gameRepository, IUnitOfWork unitOfWork)
        {
            _gameRepository = gameRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(RemoveGameCommand request, CancellationToken cancellationToken)
        {
            var gameId = request.Id;
            var gameExists = await _gameRepository.Contains(gameId);

            if (!gameExists)
            {
                throw new EntityNotFoundException(nameof(Game), gameId);
            }

            await _gameRepository.Remove(new Game { Id = gameId });
            await _unitOfWork.Commit();

            return true;
        }
    }
}
