﻿using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Games.CreateGame
{
    public class CreateGameCommandValidator : GameCommandValidator<CreateGameCommand>
    {
        public CreateGameCommandValidator(IGamingPlatformRepository gamingPlatformRepository) : base(gamingPlatformRepository) { }
    }
}
