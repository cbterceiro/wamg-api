﻿using MediatR;
using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Commands.Games.CreateGame
{
    public class CreateGameCommand : GameCommand, IRequest<int>, IMapTo<Game>
    {
    }
}
