﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Games.CreateGame
{
    public class CreateGameCommandHandler : IRequestHandler<CreateGameCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IGameRepository _gameRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateGameCommandHandler(IMapper mapper, IGameRepository gameRepository, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _gameRepository = gameRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CreateGameCommand request, CancellationToken cancellationToken)
        {
            var game = _mapper.Map<Game>(request);

            await _gameRepository.Add(game);
            await _unitOfWork.Commit();

            return game.Id;
        }
    }
}
