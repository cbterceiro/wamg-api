﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAMG.Application.Commands.Games
{
    public class GameCommand
    {
        public string Name { get; set; }
        public string Genre { get; set; }
        public int PlatformId { get; set; }
    }
}
