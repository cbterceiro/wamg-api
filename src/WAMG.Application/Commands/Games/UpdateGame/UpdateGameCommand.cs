﻿using MediatR;
using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Commands.Games.UpdateGame
{
    public class UpdateGameCommand : GameCommand, IRequest<bool>, IMapTo<Game>
    {
        public int Id { get; set; }
    }
}
