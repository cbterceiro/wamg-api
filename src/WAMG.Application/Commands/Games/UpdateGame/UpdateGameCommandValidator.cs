﻿using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Games.UpdateGame
{
    public class UpdateGameCommandValidator : GameCommandValidator<UpdateGameCommand>
    {
        public UpdateGameCommandValidator(IGamingPlatformRepository gamingPlatformRepository) : base(gamingPlatformRepository) { }
    }
}
