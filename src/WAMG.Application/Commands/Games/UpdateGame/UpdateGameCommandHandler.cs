﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Games.UpdateGame
{
    public class UpdateGameCommandHandler : IRequestHandler<UpdateGameCommand, bool>
    {
        private readonly IGameRepository _gameRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateGameCommandHandler(IGameRepository gameRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _gameRepository = gameRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(UpdateGameCommand request, CancellationToken cancellationToken)
        {
            var gameId = request.Id;
            var game = await _gameRepository.GetById(gameId);

            if (game == null)
            {
                throw new EntityNotFoundException(nameof(Game), gameId);
            }

            _mapper.Map(request, game);

            await _gameRepository.Update(game);
            await _unitOfWork.Commit();

            return true;
        }
    }
}
