﻿using FluentValidation;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Domain.Enums;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Games
{
    public class GameCommandValidator<T> : AbstractValidator<T> where T : GameCommand
    {
        protected readonly IGamingPlatformRepository _gamingPlatformRepository;

        public GameCommandValidator(IGamingPlatformRepository gamingPlatformRepository)
        {
            _gamingPlatformRepository = gamingPlatformRepository;

            RuleFor(g => g.Name)
                .NotEmpty()
                    .WithMessage("The game's name must not be empty")
                .MaximumLength(100)
                    .WithMessage("The game's name must not exceed 100 characters");

            RuleFor(g => g.Genre)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                    .WithMessage("The game's genre must not be empty")
                .IsEnumName(typeof(GameGenre))
                    .WithMessage("The game's genre was not recognized");

            RuleFor(g => g.PlatformId)
                .Cascade(CascadeMode.Stop)
                .GreaterThanOrEqualTo(1)
                    .WithMessage("The gaming platform identifier must be greater than or equal to 1")
                .MustAsync(ExistOnDatabase)
                    .WithMessage("The gaming platform {PropertyValue} was not found on the database");
        }

        private Task<bool> ExistOnDatabase(int gamingPlatformId, CancellationToken cancellationToken)
        {
            return _gamingPlatformRepository.Contains(g => g.Id == gamingPlatformId);
        }
    }
}
