﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Games.ReturnGameFromFriend
{
    public class ReturnGameFromFriendCommandHandler : IRequestHandler<ReturnGameFromFriendCommand, bool>
    {
        private readonly IGameRepository _gameRepository;
        private readonly IFriendRepository _friendRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ReturnGameFromFriendCommandHandler(IGameRepository gameRepository, IFriendRepository friendRepository, IUnitOfWork unitOfWork)
        {
            _gameRepository = gameRepository;
            _friendRepository = friendRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(ReturnGameFromFriendCommand request, CancellationToken cancellationToken)
        {
            var gameId = request.Id;
            var game = await _gameRepository.GetById(gameId);

            if (game == null)
            {
                throw new EntityNotFoundException(nameof(Game), gameId);
            }

            game.LoanedToId = null;

            await _gameRepository.Update(game);
            await _unitOfWork.Commit();

            return true;
        }
    }
}
