﻿using MediatR;

namespace WAMG.Application.Commands.Games.ReturnGameFromFriend
{
    public class ReturnGameFromFriendCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public ReturnGameFromFriendCommand() { }
        public ReturnGameFromFriendCommand(int id)
        {
            Id = id;
        }
    }
}
