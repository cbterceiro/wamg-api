﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Friends.UpdateFriend
{
    public class UpdateFriendCommandHandler : IRequestHandler<UpdateFriendCommand, bool>
    {
        private readonly IFriendRepository _friendRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateFriendCommandHandler(IFriendRepository friendRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _friendRepository = friendRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(UpdateFriendCommand request, CancellationToken cancellationToken)
        {
            var friendId = request.Id;
            var friend = await _friendRepository.GetById(friendId);

            if (friend == null)
            {
                throw new EntityNotFoundException(nameof(Friend), friendId);
            }

            _mapper.Map(request, friend);
            
            await _friendRepository.Update(friend);
            await _unitOfWork.Commit();

            return true;
        }
    }
}
