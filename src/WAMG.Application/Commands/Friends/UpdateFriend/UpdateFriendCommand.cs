﻿using MediatR;
using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Commands.Friends.UpdateFriend
{
    public class UpdateFriendCommand : FriendCommand, IRequest<bool>, IMapTo<Friend>
    {
        public int Id { get; set; }
    }
}
