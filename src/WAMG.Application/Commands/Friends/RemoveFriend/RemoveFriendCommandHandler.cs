﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Friends.RemoveFriend
{
    public class RemoveFriendCommandHandler : IRequestHandler<RemoveFriendCommand, bool>
    {
        private readonly IFriendRepository _friendRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RemoveFriendCommandHandler(IFriendRepository friendRepository, IUnitOfWork unitOfWork)
        {
            _friendRepository = friendRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(RemoveFriendCommand request, CancellationToken cancellationToken)
        {
            var friendId = request.Id;
            var friendExists = await _friendRepository.Contains(friendId);

            if (!friendExists)
            {
                throw new EntityNotFoundException(nameof(Friend), friendId);
            }

            await _friendRepository.Remove(new Friend { Id = friendId });
            await _unitOfWork.Commit();

            return true;
        }
    }
}
