﻿using MediatR;

namespace WAMG.Application.Commands.Friends.RemoveFriend
{
    public class RemoveFriendCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public RemoveFriendCommand() { }
        public RemoveFriendCommand(int id)
        {
            Id = id;
        }
    }
}
