﻿namespace WAMG.Application.Commands.Friends
{
    public class FriendCommand
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
