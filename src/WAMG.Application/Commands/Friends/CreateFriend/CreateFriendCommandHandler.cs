﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.Friends.CreateFriend
{
    public class CreateFriendCommandHandler : IRequestHandler<CreateFriendCommand, int>
    {
        private readonly IFriendRepository _friendRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateFriendCommandHandler(IFriendRepository friendRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _friendRepository = friendRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CreateFriendCommand request, CancellationToken cancellationToken)
        {
            var friend = _mapper.Map<Friend>(request);

            await _friendRepository.Add(friend);
            await _unitOfWork.Commit();

            return friend.Id;
        }
    }
}
