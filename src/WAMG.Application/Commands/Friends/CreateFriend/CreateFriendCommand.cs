﻿using MediatR;
using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Commands.Friends.CreateFriend
{
    public class CreateFriendCommand : FriendCommand, IRequest<int>, IMapTo<Friend>
    {
    }
}
