﻿using FluentValidation;

namespace WAMG.Application.Commands.Friends
{
    public abstract class FriendCommandValidator<T> : AbstractValidator<T> where T : FriendCommand
    {
        public FriendCommandValidator()
        {
            RuleFor(f => f.Name)
                .NotEmpty()
                    .WithMessage("The friend's name must not be empty")
                .MaximumLength(100)
                    .WithMessage("The friend's name must not exceed 100 characters");

            RuleFor(f => f.Email)
                .EmailAddress()
                    .WithMessage("The friend's email address must be a valid one")
                .MaximumLength(50)
                    .WithMessage("The friend's email address must not exceed 50 characters");

            RuleFor(f => f.PhoneNumber)
                .MaximumLength(20)
                    .WithMessage("The friend's phone number must not exceed 20 characters");
        }
    }
}
