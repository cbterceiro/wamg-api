﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAMG.Application.Commands.ApplicationUsers.Authenticate
{
    public class AuthenticationResponse
    {
        public string AccessToken { get; set; }
    }
}
