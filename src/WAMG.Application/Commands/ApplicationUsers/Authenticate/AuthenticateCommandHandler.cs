﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Identity;

namespace WAMG.Application.Commands.ApplicationUsers.Authenticate
{
    public class AuthenticateCommandHandler : IRequestHandler<AuthenticateCommand, AuthenticationResponse>
    {
        private readonly IIdentityService _identityService;

        public AuthenticateCommandHandler(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task<AuthenticationResponse> Handle(AuthenticateCommand request, CancellationToken cancellationToken)
        {
            var accessToken = await _identityService.AuthenticateAndGenerateToken(request.Username, request.Password);

            return new AuthenticationResponse()
            {
                AccessToken = accessToken,
            };
        }
    }
}
