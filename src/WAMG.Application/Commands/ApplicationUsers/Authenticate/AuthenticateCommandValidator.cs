﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAMG.Application.Commands.ApplicationUsers.Authenticate
{
    public class AuthenticateCommandValidator : AbstractValidator<AuthenticateCommand>
    {
        public AuthenticateCommandValidator()
        {
            RuleFor(a => a.Username)
                .NotEmpty()
                    .WithMessage("Please provide a username");

            RuleFor(a => a.Password)
                .NotEmpty()
                    .WithMessage("Please provide a password");
        }
    }
}
