﻿using FluentValidation;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.GamingPlatforms.UpdateGamingPlatform
{
    public class UpdateGamingPlatformCommandValidator : GamingPlatformCommandValidator<UpdateGamingPlatformCommand>
    {
        private readonly IGamingPlatformRepository _gamingPlatformRepository;

        public UpdateGamingPlatformCommandValidator(IGamingPlatformRepository gamingPlatformRepository)
        {
            _gamingPlatformRepository = gamingPlatformRepository;

            RuleFor(g => g.Name)
                .MustAsync((gamingPlatform, name, cancellationToken) => BeUnique(name, gamingPlatform.Id))
                    .WithMessage("There is already another gaming platform with this name");
        }

        private async Task<bool> BeUnique(string name, int id)
        {
            var alreadyExistsAnotherGamingPlatformWithThisName = await _gamingPlatformRepository.Contains(g => g.Name == name && g.Id != id);
            return !alreadyExistsAnotherGamingPlatformWithThisName;
        }
    }
}
