﻿using MediatR;
using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Commands.GamingPlatforms.UpdateGamingPlatform
{
    public class UpdateGamingPlatformCommand : GamingPlatformCommand, IRequest<bool>, IMapTo<GamingPlatform>
    {
        public int Id { get; set; }
    }
}
