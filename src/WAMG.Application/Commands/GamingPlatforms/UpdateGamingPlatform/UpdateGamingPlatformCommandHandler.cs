﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.GamingPlatforms.UpdateGamingPlatform
{
    public class UpdateGamingPlatformCommandHandler : IRequestHandler<UpdateGamingPlatformCommand, bool>
    {
        private readonly IGamingPlatformRepository _gamingPlatformRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateGamingPlatformCommandHandler(IGamingPlatformRepository gamingPlatformRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _gamingPlatformRepository = gamingPlatformRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(UpdateGamingPlatformCommand request, CancellationToken cancellationToken)
        {
            var gamingPlatformId = request.Id;
            var gamingPlatform = await _gamingPlatformRepository.GetById(gamingPlatformId);

            if (gamingPlatform == null)
            {
                throw new EntityNotFoundException(nameof(GamingPlatform), gamingPlatformId);
            }

            _mapper.Map(request, gamingPlatform);
            
            await _gamingPlatformRepository.Update(gamingPlatform);
            await _unitOfWork.Commit();

            return true;
        }
    }
}
