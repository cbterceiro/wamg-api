﻿using FluentValidation;

namespace WAMG.Application.Commands.GamingPlatforms
{
    public abstract class GamingPlatformCommandValidator<T> : AbstractValidator<T> where T : GamingPlatformCommand
    {
        public GamingPlatformCommandValidator()
        {
            RuleFor(g => g.Name)
                .NotEmpty()
                    .WithMessage("The gaming platform's name must not be empty")
                .MaximumLength(100)
                    .WithMessage("The gaming platform's name must not exceed 100 characters");
        }
    }
}
