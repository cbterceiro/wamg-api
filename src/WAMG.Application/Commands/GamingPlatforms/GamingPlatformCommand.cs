﻿namespace WAMG.Application.Commands.GamingPlatforms
{
    public class GamingPlatformCommand
    {
        public string Name { get; set; }
        public bool IsConsole { get; set; }
        public bool IsHandheld { get; set; }
    }
}
