﻿using MediatR;
using WAMG.Application.Common.Mapping;
using WAMG.Domain.Entities;

namespace WAMG.Application.Commands.GamingPlatforms.CreateGamingPlatform
{
    public class CreateGamingPlatformCommand : GamingPlatformCommand, IRequest<int>, IMapTo<GamingPlatform>
    {
    }
}
