﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.GamingPlatforms.CreateGamingPlatform
{
    public class CreateGamingPlatformCommandHandler : IRequestHandler<CreateGamingPlatformCommand, int>
    {
        private readonly IGamingPlatformRepository _gamingPlatformRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateGamingPlatformCommandHandler(IGamingPlatformRepository gamingPlatformRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _gamingPlatformRepository = gamingPlatformRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CreateGamingPlatformCommand request, CancellationToken cancellationToken)
        {
            var gamingPlatform = _mapper.Map<GamingPlatform>(request);

            await _gamingPlatformRepository.Add(gamingPlatform);
            await _unitOfWork.Commit();

            return gamingPlatform.Id;
        }
    }
}
