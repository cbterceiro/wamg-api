﻿using FluentValidation;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.GamingPlatforms.CreateGamingPlatform
{
    public class CreateGamingPlatformCommandValidator : GamingPlatformCommandValidator<CreateGamingPlatformCommand>
    {
        private readonly IGamingPlatformRepository _gamingPlatformRepository;

        public CreateGamingPlatformCommandValidator(IGamingPlatformRepository gamingPlatformRepository)
        {
            _gamingPlatformRepository = gamingPlatformRepository;

            RuleFor(g => g.Name)
                .MustAsync(BeUnique)
                    .WithMessage("There is already another gaming platform with this name");
        }

        private async Task<bool> BeUnique(string name, CancellationToken cancellationToken)
        {
            var alreadyExistsGamingPlatformWithThisName = await _gamingPlatformRepository.Contains(g => g.Name == name);
            return !alreadyExistsGamingPlatformWithThisName;
        }
    }
}
