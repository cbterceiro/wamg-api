﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using WAMG.Application.Common.Exceptions;
using WAMG.Core.Repository;
using WAMG.Domain.Entities;
using WAMG.Domain.Repository;

namespace WAMG.Application.Commands.GamingPlatforms.RemoveGamingPlatform
{
    public class RemoveGamingPlatformCommandHandler : IRequestHandler<RemoveGamingPlatformCommand, bool>
    {
        private readonly IGamingPlatformRepository _gamingPlatformRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RemoveGamingPlatformCommandHandler(IGamingPlatformRepository gamingPlatformRepository, IUnitOfWork unitOfWork)
        {
            _gamingPlatformRepository = gamingPlatformRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(RemoveGamingPlatformCommand request, CancellationToken cancellationToken)
        {
            var gamingPlatformId = request.Id;
            var gamingPlatformExists = await _gamingPlatformRepository.Contains(gamingPlatformId);

            if (!gamingPlatformExists)
            {
                throw new EntityNotFoundException(nameof(GamingPlatform), gamingPlatformId);
            }

            await _gamingPlatformRepository.Remove(new GamingPlatform { Id = gamingPlatformId });
            await _unitOfWork.Commit();

            return true;
        }
    }
}
