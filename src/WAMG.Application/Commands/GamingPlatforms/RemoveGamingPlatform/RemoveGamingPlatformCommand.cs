﻿using MediatR;

namespace WAMG.Application.Commands.GamingPlatforms.RemoveGamingPlatform
{
    public class RemoveGamingPlatformCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public RemoveGamingPlatformCommand() { }
        public RemoveGamingPlatformCommand(int id)
        {
            Id = id;
        }
    }
}
