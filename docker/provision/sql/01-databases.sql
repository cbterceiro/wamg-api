# create databases
CREATE DATABASE IF NOT EXISTS `wamg`;
CREATE DATABASE IF NOT EXISTS `wamg_tests`;

# create wamg api user and grant rights on wamg database
USE wamg;
CREATE USER IF NOT EXISTS 'wamg_api_user'@'%' IDENTIFIED BY 'kXPGvHz5fWt2E6ju8PaUQXQBjXUEYET9';
GRANT ALL ON wamg.* TO 'wamg_api_user'@'%';

# create wamg tests user and grant rights on wamg-tests database
USE wamg_tests;
CREATE USER IF NOT EXISTS 'wamg_tests_user'@'%' IDENTIFIED BY 'p3CueYeMvUrByTRxGSXJFMEnwKHjkqUF';
GRANT ALL ON wamg_tests.* TO 'wamg_tests_user'@'%';
